#!/usr/bin/python
# coding: utf-8

"""
Copyright 2011 Risto H. Kurppa risto at kurppa . fi under the GPLv3+ License
	  2012 Timo Rauhala trauhala at gmail . com under the GPLv3+ License

GPL3 LICENSE
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
#Extra line just to test git merge capabilities :)
# Thanks :)
# -T
# Tested, working!




import os
import sys
import tetherfactory
import ConfigParser
import logging
import threading
import Queue
import scan
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4 import uic
import time
try:
    from editor import Editor 
except ImportError, Exception:
    pass
try:
    import smtplib
except ImportError:
    pass
import urllib2
import cherrypy


class GUI(QtGui.QMainWindow):
  
    def __init__(self, commandqueue, endcommand, config, *args):
        super(GUI, self, *args).__init__()

        #Setup queue for the serial reader thread
        self.commandqueue = commandqueue
        #Setup endcommand for the serial reader thread
        self.endcommand = endcommand
        #Load UI file and show GUI
        self.Config = config
        self.ui = uic.loadUi("tfqtgui_tabs.ui", self)
        self.ui.show()
        
        self.autorunenabled = False
        self.inspectionmalfuction = False
        self.endofcycle = False #True when last cycleposition received
        self.wedgedown = False # True if wedge down in wrong cycle
        self.useinspection = False #Hold value of checkBox_inspectionsystem
        self.email = False #Hold value of send_email_checkbox
        self.parameters_sent = False #Check if params have been sent to arduino
        self.parameters_same = True #Compare current parameters to the one that was loaded
        self.servosattached = False
        self.clamp1closedmanual = False #If clamps closed manually disable spooling
        self.clamp2closedmanual = False #If clamps closed manually disable spooling
        self.stoppedonbadbond = False #True when inspection reports a failed bond 
        self.autofix = False #True when autofixing a bad bond
        self.autofixedbond = False #True when the last bond made was an autofix
        self.bondsafterautofix = 0
        self.autofixedbonds = 0
        self.stoppedonbadbondtime = ""
        self.stoppedonwedgedowntime = ""
        self.inspectionmalfunctiontime = ""
        #Runtime variables for the Serial reader Thread
        self.connection = 0
        self.sidesteps = 0  #keep count of sidesteps for the progressbar
        self.sidetepmax = 18000 #max ammout of sidesteps, for the progressbar
        self.sidestepdirection = 1 #direction for progressbar movement
        self.layer = 1
        


     
        
       
            
        

    ##INIT DEFAULT PROCESS PARAMETERS
        self.ui.input_stepdelay.setValue(300)
        self.ui.input_clamp1open.setValue(130)
        self.ui.input_clamp1close.setValue(60)
        self.ui.input_clamp2close.setValue(50)
        self.ui.input_clamp2open.setValue(179)
        self.ui.input_tensionoff.setValue(80)
        self.ui.input_tensionon.setValue(20)
        self.ui.input_flatteneron.setValue(179)
        self.ui.input_flatteneroff.setValue(0)
        self.ui.input_pinon.setValue(123)
        self.ui.input_pinoff.setValue(133)
        self.ui.input_spoolsteps.setValue(40)
        self.ui.input_stepperrpm.setValue(5)
        self.ui.input_spoolsidesteps.setValue(40)
        self.ui.input_tfsidewayrpm.setValue(40)
        self.ui.input_tfsidewaydistance.setValue(5)

        self.ui.groupBox_configmanual.setDisabled(1)
#        self.ui.groupBox_manual.setDisabled(1)
        self.ui.groupBox_autorun.setDisabled(1)
#       self.ui.groupBox_autorun.setDisabled(1)
#       self.ui.btn_autorun_stop.setDisabled(1)
        self.ui.disconnectbutton.setDisabled(1)
        self.ui.btn_checkbadbonds.setDisabled(1)
        self.ui.btn_attachservos.setDisabled(1)
        self.ui.btn_detachservos.setDisabled(1) 
       #Show the default tabs on start 
        self.ui.tabWidget.setCurrentIndex(0)

        
        

###GENERIC
        ##MENU
        self.connect(self.ui.actionQuit, QtCore.SIGNAL('triggered()'), self.close)
        self.connect(self.ui.actionOpen, QtCore.SIGNAL('triggered()'), self.openconfig)
        self.connect(self.ui.actionSave, QtCore.SIGNAL('triggered()'), self.saveconfig)
        self.connect(self.ui.actionSave_As, QtCore.SIGNAL('triggered()'), self.saveconfigas)

        self.connect(self.ui.actionShow_log, QtCore.SIGNAL('triggered()'), self.showlog)

        ##MESSAGE TEXT BOX
        self.ui.textbrowser.ensureCursorVisible()
        self.ui.textbrowser.append('Welcome to Tether Factory control software!')

####ARDUINO CONTROL UI
        #INIT
        self.connect(self.ui.initbutton, QtCore.SIGNAL('clicked()'), self.fun_init_serial)
        #CLOSE SERIAL
        self.connect(self.ui.disconnectbutton, QtCore.SIGNAL('clicked()'), self.fun_close_serial)
        self.connect(self.ui.btn_sendparameters, QtCore.SIGNAL('clicked()'), self.fun_sendparameters)
        #ATTACH / DETACH SERVOS
        self.connect(self.ui.btn_attachservos, QtCore.SIGNAL('clicked()'), lambda: self.fun_attachservos(True))
        self.connect(self.ui.btn_detachservos, QtCore.SIGNAL('clicked()'), lambda: self.fun_attachservos(False))
        
###MANUAL PROCESS CONTROL
        #Direct input textboxes
        self.connect(self.ui.cmdinput, QtCore.SIGNAL("returnPressed()"), self.commandinput)
        self.connect(self.ui.valueinput, QtCore.SIGNAL("returnPressed()"), self.commandinput)

###RESET INDICATORS
        self.connect(self.ui.btn_resetcounter, QtCore.SIGNAL('clicked()'), self.fun_resetBondCount)
        self.connect(self.ui.btn_resetspool, QtCore.SIGNAL('clicked()'), self.fun_resetSpoolMovement)
        self.connect(self.ui.btn_resettetherlength, QtCore.SIGNAL('clicked()'), self.fun_resetTetherLength)

###AUTORUN CONTROL
        #NEXT STEP
        self.connect(self.ui.btn_nextStep, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(100, 0))
        #AUTORUN
        self.connect(self.ui.btn_autorun_start, QtCore.SIGNAL('clicked()'), self.fun_autorun_start)
        self.connect(self.ui.btn_autorun_stop, QtCore.SIGNAL('clicked()'), self.fun_autorun_stop)
        
        #INITIALIZE
        self.connect(self.ui.btn_initializetf, QtCore.SIGNAL('clicked()'), self.fun_setinitposition)

##CONFIG & MANUAL CONTROL

        #clamp1
        self.connect(self.ui.btn_openclamp1, QtCore.SIGNAL('clicked()'), lambda: self.openCloseClamps(1, 'open'))
        self.connect(self.ui.btn_closeclamp1, QtCore.SIGNAL('clicked()'), lambda: self.openCloseClamps(1, 'close'))
        #clamp2
        self.connect(self.ui.btn_openclamp2, QtCore.SIGNAL('clicked()'), lambda: self.openCloseClamps(2, 'open'))
        self.connect(self.ui.btn_closeclamp2, QtCore.SIGNAL('clicked()'), lambda: self.openCloseClamps(2, 'close'))
        #tension
        self.connect(self.ui.btn_tensionon, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(220, self.ui.input_tensionon.value()))
        self.connect(self.ui.btn_tensionoff, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(390, self.ui.input_tensionoff.value()))
        #flattener
        self.connect(self.ui.btn_flattenerflatten, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(240, self.ui.input_flatteneron.value()))
        self.connect(self.ui.btn_flattenerrelease, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(250, self.ui.input_flatteneroff.value()))
        #flattenerarm
        self.connect(self.ui.btn_flattenerarmin, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(230, self.ui.input_flattenerarmin.value()))
        self.connect(self.ui.btn_flattenerarmout, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(260, self.ui.input_flattenerarmout.value()))
        #pin
        self.connect(self.ui.btn_pinshow, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(270, self.ui.input_pinon.value()))
        self.connect(self.ui.btn_pinremove, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(380, self.ui.input_pinoff.value()))
        #bonderclamp
        self.connect(self.ui.btn_bonderclampopen, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(350, 0))
        self.connect(self.ui.btn_bonderclampclose, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(280, 0))
        #spool
        self.connect(self.ui.btn_spool, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(420, 0))
        self.connect(self.ui.btn_spoolmoveside, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(430, 0))

       #Bonder btn
        self.connect(self.ui.btn_bonderswitch_down, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(340, 0))
        self.connect(self.ui.btn_bonderswitch_up, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(360, 0))

       #Tether factory position buttons
        self.connect(self.ui.btn_tfpositionleft, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(820, 0))
        self.connect(self.ui.btn_tfpositioncenter, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(810, 0))
        self.connect(self.ui.btn_tfpositionright, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(800, 0))
        self.connect(self.ui.btn_tfpositioncalib_moveleft, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(830, 0))
        self.connect(self.ui.btn_tfpositioncalib_moveright, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(840, 0))
        self.connect(self.ui.btn_releasewireselect, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(792, 0))


        #Autofix bad bond button
        self.connect(self.ui.btn_autofix, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(870, self.ui.input_autofixsteps.value()))

        #Bad bond checking
        self.connect(self.ui.btn_checkbadbonds, QtCore.SIGNAL('clicked()'), lambda: self.fun_send(165, 0))
        
       #Scan for Arduino(s) 
#        self.checkports()

        #LOGGING
        self.ui.Log_checkBox.stateChanged.connect(self.fun_changeLogging)

        #EMAIL USER, IF FAILED BOND
        self.ui.send_email_checkbox.stateChanged.connect(self.fun_changeEmail)
        
        #CHECK FOR INSPECITON SYSTEM CHECKBOX
        self.ui.checkBox_inspectionsystem.stateChanged.connect(self.fun_changeInspectionCheck)
        
        
        ## CONNECT INPUT FIELDS TO INFORM USER IF PARAMS HAVE CHANGED
        self.ui.input_clamp1open.valueChanged.connect(self.parameters_changed)
        self.ui.input_clamp1close.valueChanged.connect(self.parameters_changed)
        self.ui.input_clamp2open.valueChanged.connect(self.parameters_changed)
        self.ui.input_clamp2close.valueChanged.connect(self.parameters_changed)
        self.ui.input_tensionoff.valueChanged.connect(self.parameters_changed)
        self.ui.input_tensionon.valueChanged.connect(self.parameters_changed)

        self.ui.input_flattenerarmout.valueChanged.connect(self.parameters_changed)
        self.ui.input_flattenerarmin.valueChanged.connect(self.parameters_changed)
        self.ui.input_flatteneroff.valueChanged.connect(self.parameters_changed)
        self.ui.input_flatteneron.valueChanged.connect(self.parameters_changed)
        self.ui.input_pinoff.valueChanged.connect(self.parameters_changed)
        self.ui.input_pinon.valueChanged.connect(self.parameters_changed)

        self.ui.input_spoolsteps.valueChanged.connect(self.parameters_changed)
        self.ui.input_stepperrpm.valueChanged.connect(self.parameters_changed)
        self.ui.input_spoolsidefreearea.valueChanged.connect(self.parameters_changed)
        self.ui.input_spoolsidesteps.valueChanged.connect(self.parameters_changed)
        self.ui.input_spoolsiderpm.valueChanged.connect(self.parameters_changed)
        self.ui.input_tfsidewaydistance.valueChanged.connect(self.parameters_changed)
        self.ui.input_tfsidewayrpm.valueChanged.connect(self.parameters_changed)
        
        self.ui.input_spoolsidewaycorrection.valueChanged.connect(self.parameters_changed)
        
## MANUAL COMMAND & VALUE INPUT
    def commandinput(self):
        self.fun_send(self.ui.cmdinput.text(), self.ui.valueinput.text())
        self.ui.cmdinput.clear()
        self.ui.valueinput.clear()

##SELECTING SERIAL PORT DEVICE BASED ON THE GUI COMBO BOX
    def serialport(self):
       # self.msg.write(self.defaultserialdevice)
        device=""
        if (self.ui.combo_serialport.currentIndex() == 0):
            device="/dev/ttyUSB0"
        elif(self.ui.combo_serialport.currentIndex() == 1):
            device="/dev/ttyUSB1"
        elif(self.ui.combo_serialport.currentIndex() == 2):
            device="/dev/ttyUSB2"
        elif(self.ui.combo_serialport.currentIndex() == 3):
            device="/dev/ttyACM0"
        elif(self.ui.combo_serialport.currentIndex() == 4):
            device="/dev/ttyACM1"
        elif(self.ui.combo_serialport.currentIndex() == 5):
            device="COM1"
        elif(self.ui.combo_serialport.currentIndex() == 6):
            device="COM2"
        elif(self.ui.combo_serialport.currentIndex() == 7):
            device="COM3"
        elif(self.ui.combo_serialport.currentIndex() == 8):
            device="COM4"
        elif(self.ui.combo_serialport.currentIndex() == 9):
            device="COM5"
        elif(self.ui.combo_serialport.currentIndex() == 10):
            device="COM6"
        elif(self.ui.combo_serialport.currentIndex() == 11):
            device="COM7"
        elif(self.ui.combo_serialport.currentIndex() == 12):
            device="COM8"
        elif(self.ui.combo_serialport.currentIndex() == 13):
            device="COM9"
        elif(self.ui.combo_serialport.currentIndex() == 14):
            device="COM10"

        return device
    #    tf.msg.write(self.defaultserialdevice)
        
##FUNCTION FOR SCANNING THE SERIAL PORTS FOR ARDUINO
    def checkports(self):
        openports = scan.ports()
        for x in range(len(openports)):
            openports.reverse()
            defaultdevice = openports[0]
            self.write('Serial device found on port ' + openports.pop())
            #print defaultdevice
            if defaultdevice == "COM5":
                self.ui.combo_serialport.setCurrentIndex(9)

##FUNCTION TO POINT stdout.write TO TEXTBROWSER
    def write(self, msg):
        print msg
        self.ui.textbrowser.append(msg)
        tf.logger.info(msg)

##INITIALIZING SERIAL PORT
    def fun_init_serial(self):
        device=self.serialport()
        self.ui.groupBox_configmanual.setEnabled(1)
        self.ui.disconnectbutton.setEnabled(1)
        self.ui.btn_checkbadbonds.setEnabled(1)        
        self.ui.initbutton.setDisabled(1)
        self.ui.combo_serialport.setDisabled(1)
        self.ui.btn_attachservos.setEnabled(1)
        tf.initialize(device)
        QtCore.QCoreApplication.processEvents() #Flush the event queue before sleeping
        self.connection = 1 #BondCounterThread knows to start polling.
        tf.logger.info("Serial connection opened to " +device +"!")
#        time.sleep(1)
#        print "kukkuu!"
        self.fun_pollInitValues()

 
##CLOSING SERIAL PORT
    def fun_close_serial(self):
        self.connection = 0 #BondCounterThread knows to stop polling.
        self.ui.groupBox_configmanual.setDisabled(1)
        self.ui.groupBox_autorun.setDisabled(1)
        self.ui.combo_serialport.setEnabled(1)
        self.ui.initbutton.setEnabled(1)
        self.ui.disconnectbutton.setDisabled(1)
        self.ui.btn_attachservos.setEnabled(1)
        self.ui.btn_detachservos.setDisabled(1)
        self.ui.btn_tfpositioncenter.setStyleSheet('QPushButton')
        self.ui.wireIndicatorCenter.setStyleSheet('QPushButton')
        self.ui.btn_tfpositionright.setStyleSheet('QPushButton')
        self.ui.wireIndicatorRight.setStyleSheet('QPushButton')     ## Reset wireindicators on disconnect
        self.ui.btn_tfpositionleft.setStyleSheet('QPushButton')
        self.ui.wireIndicatorLeft.setStyleSheet('QPushButton')
        QtCore.QCoreApplication.processEvents() #Flush the event queue before sleeping
        if self.servosattached: # Detach servos when disconnecting TF
            self.fun_attachservos(False)
            print "on ne pois"
        self.parameters_sent = False
        QtCore.QCoreApplication.processEvents() #Flush the event queue before sleeping
        time.sleep(1)
        QtCore.QCoreApplication.processEvents() #Flush the event queue before sleeping
        tf.close()
        tf.logger.info("Serial connection closed.")
        
  
##SEND COMMAND OVER SERIAL PORT
    def fun_send(self, command, value):
        tf.send(command,value)


##POLL ARDUINO FOR INITIAL VALUES
    def fun_pollInitValues(self):
        time.sleep(1)
        self.fun_send(111,0) #Ask arduino about its rank
#        time.sleep(1)
        self.fun_send(112,0) #Ask arduino about the wire

## INITIALIZE TF, ASK USER TO REBOOT BONDER
    def fun_setinitposition(self):
        print "init"
        init_msg = "Initializing TF. Reset bonder and press OK. Run 'next step' once."
        reply = QtGui.QMessageBox.question(self, 'Message', 
                         init_msg, QtGui.QMessageBox.Ok, QtGui.QMessageBox.Cancel)
    
        if reply == QtGui.QMessageBox.Ok:
            self.fun_send(150, 0)
        else:
            pass
        
        
## CHANGE THE LEVEL OF EVENTS TO BE LOGGED
    def fun_changeLogging(self):
        if self.ui.Log_checkBox.isChecked():
            tf.changeLogLevel(0)
            tf.logger.info("Logging level changed to INFO")
        else:
            tf.changeLogLevel(1)
            tf.logger.info("Logging level changed to WARNING")

## ATTACH / DETACH SERVOS
    def fun_attachservos(self, servoconnection):
        if (servoconnection):
            self.wedgedown = False #Assume user is smart enough NOT to attach servos before fixing wedge down problem
            if self.parameters_sent and self.parameters_same:
                self.ui.btn_attachservos.setDisabled(1)
                self.ui.btn_detachservos.setEnabled(1)
                self.ui.btn_autorun_start.setEnabled(1)
                self.ui.groupBox_autorun.setEnabled(1)
                self.ui.autofix_indicator.setStyleSheet('QLabel')
                self.ui.autofix_indicator.setText('')                
                self.fun_send(180, 1)
                self.servosattached = True
                self.write("Attaching servos..")
            elif not self.parameters_sent or not self.parameters_same:
                param_msg = "Parameters not sent. \nSend parameters before connecting servos?"
                reply = QtGui.QMessageBox.question(self, 'Parameters not sent!', 
                         param_msg, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No, QtGui.QMessageBox.Cancel)
    
                if reply == QtGui.QMessageBox.Yes:
                    self.fun_sendparameters()
                    time.sleep(0.2)
                    self.write("Sending parameters..")
                    QtCore.QCoreApplication.processEvents() #Flush the event queue 
                    self.fun_send(180, 1)
                    self.write("Parameters sent, attaching servos..")
                    self.servosattached = True
                    self.ui.btn_attachservos.setDisabled(1)
                    self.ui.btn_detachservos.setEnabled(1)
                    self.ui.btn_autorun_start.setEnabled(1)
                    self.ui.groupBox_autorun.setEnabled(1)
                    
                if reply == QtGui.QMessageBox.No:
                    self.write("Not sending parameters, attaching servos..")
                    self.fun_send(180, 1)
                    self.servosattached = True
                    self.ui.btn_attachservos.setDisabled(1)
                    self.ui.btn_detachservos.setEnabled(1)
                    self.ui.btn_autorun_start.setEnabled(1)
                    self.ui.groupBox_autorun.setEnabled(1)

        elif not (servoconnection):
            self.ui.btn_attachservos.setEnabled(1)
            self.ui.btn_detachservos.setDisabled(1)
            self.ui.btn_autorun_start.setDisabled(1)
            self.ui.groupBox_autorun.setDisabled(1)
            self.fun_send(180, 2)
            self.servosattached = False
            self.write("Detaching servos..")

## CLOSE/OPEN CLAMPS
    def openCloseClamps(self, clampnro, openclose):
        if clampnro == 1:
            if openclose == 'open': #open clamp 1
                print 'clamp1 open'
                self.clamp1closedmanual = False
                self.fun_send(400, self.ui.input_clamp1open.value())
            elif openclose == 'close': #close clamp 1
                print 'clamp1 closed'
                self.clamp1closedmanual = True
                self.fun_send(200, self.ui.input_clamp1close.value())
                self.ui.btn_spool.setStyleSheet('QPushButton {background-color: red}')
        elif clampnro == 2:
            if openclose == 'open': #open clamp 2
                print 'clamp2 open'
                self.clamp2closedmanual = False
                self.fun_send(410, self.ui.input_clamp2open.value())
            elif openclose == 'close': #close clamp 2
                print 'clamp2 closed'
                self.clamp2closedmanual = True
                self.fun_send(210, self.ui.input_clamp2close.value())
                self.ui.btn_spool.setStyleSheet('QPushButton {background-color: red}')
                
        if self.clamp1closedmanual == False and self.clamp2closedmanual == False:
            self.ui.btn_spool.setStyleSheet('QPushButton')
            

## CHANGE THE LEVEL OF EVENTS TO BE LOGGED
    def fun_changeEmail(self):
        if self.ui.send_email_checkbox.isChecked():
            self.email = True
            tf.logger.info("TF will send emails to user")
        else:
            self.email = False
            tf.logger.info("TF will NOT send emails to user")

##HANDLE THE LCD DISPLAYS ON GUI
    def fun_changeLcdCount(self, lcdnumber ,value):
        if lcdnumber == 1:
            self.ui.lcdNumber.display(value)
        elif lcdnumber == 2:
            self.ui.lcd_bondfailcount.display(value)


##SHOW THE AMOUNT OF TETHER SPOOLED
    def fun_showTetherLength(self, value):
        self.mmPerStep = 0.312
        self.currentsteps = value
#        print self.currentsteps
        self.tetherLength = self.currentsteps*self.mmPerStep
        if (self.tetherLength <= 100):
            self.ui.tetherLengthLcd.display(self.tetherLength)
            self.ui.lenghtUnitDisplay.setText("mm")
        elif (self.tetherLength > 1000):
            self.ui.tetherLengthLcd.display(self.tetherLength/1000)
            self.ui.lenghtUnitDisplay.setText("m")         
        elif (self.tetherLength >= 100):
            self.ui.tetherLengthLcd.display(self.tetherLength/10)
            self.ui.lenghtUnitDisplay.setText("cm")            
           
#        print value
        


##SHOW THE WIRE NUMBER IN THE GUI
    def fun_showWire(self, wirenumber):
#        self.write("Wire number: " + str(wirenumber))
        if (wirenumber == 3):
            self.ui.btn_tfpositionleft.setDisabled(True)
            self.ui.btn_tfpositionleft.setStyleSheet('QPushButton {background-color: lime}')
            self.ui.wireIndicatorLeft.setStyleSheet('QLabel {background-color: lime}')
            self.ui.btn_tfpositioncenter.setDisabled(False)
            self.ui.btn_tfpositioncenter.setStyleSheet('QPushButton')
            self.ui.wireIndicatorCenter.setStyleSheet('QLabel')
            self.ui.btn_tfpositionright.setDisabled(False)
            self.ui.btn_tfpositionright.setStyleSheet('QPushButton')
            self.ui.wireIndicatorRight.setStyleSheet('QLabel')
        elif (wirenumber == 2):
            self.ui.btn_tfpositionleft.setDisabled(False)
            self.ui.btn_tfpositionleft.setStyleSheet('QPushButton')
            self.ui.wireIndicatorLeft.setStyleSheet('QLabel')
            self.ui.btn_tfpositioncenter.setDisabled(True)
            self.ui.btn_tfpositioncenter.setStyleSheet('QPushButton {background-color: lime}')
            self.ui.wireIndicatorCenter.setStyleSheet('QLabel {background-color: lime}')
            self.ui.btn_tfpositionright.setDisabled(False)
            self.ui.btn_tfpositionright.setStyleSheet('QPushButton')
            self.ui.wireIndicatorRight.setStyleSheet('QLabel')
        elif (wirenumber == 1):
            self.ui.btn_tfpositionleft.setDisabled(False)
            self.ui.btn_tfpositionleft.setStyleSheet('QPushButton')
            self.ui.wireIndicatorLeft.setStyleSheet('QLabel')
            self.ui.btn_tfpositioncenter.setDisabled(False)
            self.ui.btn_tfpositioncenter.setStyleSheet('QPushButton')
            self.ui.wireIndicatorCenter.setStyleSheet('QLabel')
            self.ui.btn_tfpositionright.setDisabled(True)
            self.ui.btn_tfpositionright.setStyleSheet('QPushButton {background-color: lime}')
            self.ui.wireIndicatorRight.setStyleSheet('QLabel {background-color: lime}')

##SHOW SIDEWAYS MOVEMENT ON GUI
    def fun_showSpoolSidewayMovement(self, value): # Written to mimic the behavior of current arduino code. 
        print value                        # Not optimal in any way.
        print self.sidestepdirection
        self.sidesteps = value
        

        if (self.sidestepdirection == 1):
			self.ui.progressBar.setInvertedAppearance(False)	##Left to right
			self.ui.progressBar.setValue(self.sidesteps)
			self.ui.spool_position_label.setText(str(self.sidesteps) + " steps, layer " + str(self.layer)+ "\nMultiplier OFF")

        if (self.sidesteps==self.sidetepmax and self.sidestepdirection == 1):
            self.sidestepdirection=-1
            self.layer = self.layer + 1

        if (self.sidestepdirection == -1):
			self.ui.progressBar.setInvertedAppearance(True)	 ## Right to left
			self.ui.progressBar.setValue(self.sidetepmax-self.sidesteps)
			self.ui.spool_position_label.setText(str((self.sidetepmax-self.sidesteps)) + " steps, layer " + str(self.layer) + "\nMultiplier ON")

        if (self.sidesteps==0 and self.sidestepdirection == -1):
            self.sidestepdirection=1
            self.layer = self.layer + 1
        	

        
##RESET BOND COUNT
    def fun_resetBondCount(self):
        self.fun_send(770,0)  

##RESET Tether Length
    def fun_resetTetherLength(self):
        self.fun_send(775,0)  
        

##RESET SPOOL POSITION
    def fun_resetSpoolMovement(self):
        self.sidesteps = 0 
        self.sidestepdirection = 1 
        self.layer = 0
        self.fun_send(850,0)

##STOP FOR FAILED BONDS AND INFORM THE USER
    def fun_badBondStop(self, data):
    	if self.autorunenabled:
            if not self.autofixedbond and not self.autofix:
                self.fun_changeLcdCount(2, data)
                self.fun_autofix(0)
            else:
                self.autorunenabled = False
                self.stoppedonbadbond = True
                self.autofixedbond = False
                self.autofix = False
                self.stoppedonbadbondtime = time.asctime(time.localtime())
                self.fun_changeLcdCount(2, data)
                self.write("BAD BOND! Count: " +str(data) + " Stopping autorun mode!")
                QtCore.QCoreApplication.processEvents() #Flush the event queue before trying to send email
                self.ui.groupBox_configmanual.setEnabled(1) #Enable manual controls as arduino has already stopped the autorun mode
                self.ui.disconnectbutton.setEnabled(1)
                self.fun_attachservos(0) #Dettach servos if failed bond 
                self.ui.groupBox_configloadsave.setEnabled(1)
                self.ui.btn_initializetf.setEnabled(1)
                self.ui.btn_nextStep.setEnabled(1)
                QtGui.QSound('Bondfail.wav').play()
                self.fun_email_user(1)
        else:
            self.write("Autofix failed!")
            self.ui.autofix_indicator.setStyleSheet('QLabel')
            self.ui.autofix_indicator.setText('')


### STOP AUTORUN FOR WRONG MOVEMENT OF WEDGE
    def fun_wedgeDownStop(self, data):
        self.write("WEDGE DOWN IN WRONG CYCLE! STOPPING AUTORUN!")
        tf.logger.warning("WEDGE DOWN IN WRONG CYCLE!")
        self.fun_autorun_stop()
        self.fun_attachservos(0)
        self.stoppedonwedgedowntime = time.asctime(time.localtime())
        self.wedgedown = True
        self.fun_email_user(2)
        self.bondsafterautofix = 0 # Reset 10 bonds after autofix
        self.autofix = False # Reset 10 bonds after autofix
        self.autofixedbond = False # Reset 10 bonds after autofix

### EMAIL USER FOR STATUS INFORMATION
    def fun_email_user(self, reason):
        if self.networkAvailable() and self.email == True:
            self.write("Sending email..")
            QtCore.QCoreApplication.processEvents()
            to = 'tetherfactorymessenger@gmail.com'
            #to = 'trauhala@gmail.com'
            gmail_user = 'tetherfactorymessenger@gmail.com'
            gmail_pwd = 'TFpassword'
            smtpserver = smtplib.SMTP("smtp.gmail.com",587)
            smtpserver.ehlo()
            smtpserver.starttls()
            smtpserver.ehlo
            smtpserver.login(gmail_user, gmail_pwd)
            header = 'To:' + to + '\n' + 'From: ' + gmail_user + '\n' + 'Subject:TF has stopped! \n'
            if reason == 1:
                msg = (header + '\n A bond has failed! '+self.stoppedonbadbondtime +'\n Current state:\n\n'
                               '\n Bonds:'+ str(int(self.ui.lcdNumber.value())) + 
                               '\n Lenght:'+ str(int(self.ui.tetherLengthLcd.value())) +
                               '\n # of failed bonds: '+ str(int(self.ui.lcd_bondfailcount.value())) +  
                               '\n # autofixed bonds: '+ str(self.autofixedbonds) +
                               '\n'
                               )
            elif reason == 2:
                msg = (header + '\n Wedge down! '+self.stoppedonwedgedowntime +'\n Current state:\n\n'
                               '\n Bonds:'+ str(int(self.ui.lcdNumber.value())) + 
                               '\n Lenght:'+ str(int(self.ui.tetherLengthLcd.value())) +
                               '\n # of failed bonds:'+ str(int(self.ui.lcd_bondfailcount.value())) +  
                               '\n # autofixed bonds: '+ str(self.autofixedbonds) +
                               '\n'
                               )
            elif reason == 3:
                msg = (header + '\n Inspection system malfunction! '+self.inspectionmalfunctiontime +'\n Current state:\n\n'
                               '\n Bonds:'+ str(int(self.ui.lcdNumber.value())) + 
                               '\n Lenght:'+ str(int(self.ui.tetherLengthLcd.value())) +
                               '\n # of failed bonds:'+ str(int(self.ui.lcd_bondfailcount.value())) +  
                               '\n # autofixed bonds: '+ str(self.autofixedbonds) +
                               '\n'
                               )
            smtpserver.sendmail(gmail_user, to, msg)
            smtpserver.close()
            self.write("..email sent to " + to)
        elif self.email == True:
            self.write("Email not sent; no internet connection.")
        else:
            pass


### AUTOFIX BAD BONDS
    def fun_autofix(self, val):
    	if val == 0:
            self.autofix = True
            self.autofixedbond = True
            self.ui.autofix_indicator.setStyleSheet('QLabel {background-color: yellow}')
            self.ui.autofix_indicator.setText('Autofix in progress..')
            tf.logger.warning("Starting autofix for bad bonds")
            self.write("BAD BOND! Starting autofix in 10 seconds..")
            QtCore.QCoreApplication.processEvents() #Flush the event queue before sleeping
            time.sleep(10)
            self.fun_send(870, self.ui.input_autofixsteps.value())
    	elif val == 1:
            if self.autorunenabled:
                self.autofixedbond = False
                self.write("..Autofix succeeded!")
                self.write("Restarting autorun..")
                self.autofixedbonds+=1
                QtCore.QCoreApplication.processEvents() #Flush the event queue before sleeping
                time.sleep(2)
                self.fun_autorun_start()
                self.ui.autofix_indicator.setStyleSheet('QLabel')
                self.ui.autofix_indicator.setText('')
            else:
                self.write("Autofix succeeded!")
                self.ui.autofix_indicator.setStyleSheet('QLabel')
                self.ui.autofix_indicator.setText('')
        elif val == 2:
            self.bondsafterautofix+=1
            if self.bondsafterautofix > 9:
                self.bondsafterautofix = 0
                self.autofix = False
            print "bonds after: " + str(self.bondsafterautofix)
            print self.autofix
                                    
### CHECK INSPECTION SYSTEM STATUS
    def fun_checkinspection(self, value):
        if value == 1:
            self.ui.inspectionsystem_indicator.setStyleSheet('QLabel {background-color: lime}')
            self.ui.inspectionsystem_indicator.setText('Online')
            self.inspectionmalfuction = False
        elif value == 0:
            self.ui.inspectionsystem_indicator.setStyleSheet('QLabel {background-color: red}')
            self.ui.inspectionsystem_indicator.setText('Offline')
            self.write("Inspection system offline!")
            tf.logger.warning("Inspection system offline! Stopping autorun mode!")
            self.inspectionmalfuction = True #Used to stop process on showCyclePosition
            self.inspectionmalfunctiontime = time.asctime(time.localtime())
            self.fun_email_user(3)


### CHECKBOX TO CONTROL IF INSPECTION SYSTEM STATUS IS INSPECTED            
    def fun_changeInspectionCheck(self):
        if self.ui.checkBox_inspectionsystem.isChecked():
            self.useinspection = True
            tf.logger.info("TF will check inspection system status during autorun")
            self.ui.inspectionsystem_indicator.setStyleSheet('QLabel {background-color: yellow}')
            self.ui.inspectionsystem_indicator.setText('waiting..')
        else:
            self.useinspection = False
            tf.logger.info("TF will NOT CHECK inspection system status during autorun")
            self.ui.inspectionsystem_indicator.setStyleSheet('QLabel')
            self.ui.inspectionsystem_indicator.setText('')
            self.inspectionmalfuction =False
        
            
            
### START AUTORUN
    def fun_autorun_start(self):
        self.fun_send(610, self.ui.input_stepdelay.value())
        self.fun_send(130, 0)
        self.ui.groupBox_configmanual.setDisabled(1)
        self.ui.groupBox_configloadsave.setDisabled(1)
        self.ui.disconnectbutton.setDisabled(1)
        self.ui.btn_initializetf.setDisabled(1)
        self.ui.btn_nextStep.setDisabled(1)
        self.autorunenabled = True
        self.stoppedonbadbond = False

## STOP AUTORUN
    def fun_autorun_stop(self):
        self.fun_send(140, 0)
        self.ui.groupBox_configmanual.setEnabled(1)
        self.ui.disconnectbutton.setEnabled(1)
        self.ui.groupBox_configloadsave.setEnabled(1)
        self.ui.btn_initializetf.setEnabled(1)
        self.ui.btn_nextStep.setEnabled(1)
        self.autorunenabled = False
        
##SEND CONFIG PARAMETERS TO ARDUINO
    def fun_sendparameters(self):
        tf.msg.write("Sending new parameters")
        delaybetweensending=0.15
        self.fun_send(610, self.ui.input_stepdelay.value())
        time.sleep(delaybetweensending)
        self.fun_send(640, self.ui.input_clamp1open.value())
        time.sleep(delaybetweensending)
        self.fun_send(650, self.ui.input_clamp1close.value())
        time.sleep(delaybetweensending)        
        self.fun_send(730, self.ui.input_clamp2close.value())
        time.sleep(delaybetweensending)        
        self.fun_send(720, self.ui.input_clamp2open.value()) 
        time.sleep(delaybetweensending)        
        self.fun_send(660, self.ui.input_tensionoff.value()) 
        time.sleep(delaybetweensending)        
        self.fun_send(670, self.ui.input_tensionon.value())
        time.sleep(delaybetweensending)        
        self.fun_send(690, self.ui.input_flatteneron.value()) 
        time.sleep(delaybetweensending)        
        self.fun_send(680, self.ui.input_flatteneroff.value())
        time.sleep(delaybetweensending)        
        self.fun_send(685, self.ui.input_flattenerarmin.value()) 
        time.sleep(delaybetweensending)        
        self.fun_send(695, self.ui.input_flattenerarmout.value())
        time.sleep(delaybetweensending)        
        self.fun_send(710, self.ui.input_pinon.value())
        time.sleep(delaybetweensending)        
        self.fun_send(700, self.ui.input_pinoff.value())
        time.sleep(delaybetweensending)        
        self.fun_send(630, self.ui.input_spoolsteps.value())
        time.sleep(delaybetweensending)        
        self.fun_send(620, self.ui.input_stepperrpm.value())
        time.sleep(delaybetweensending)        
        self.fun_send(740, self.ui.input_spoolsidesteps.value())
        time.sleep(delaybetweensending)        
        self.fun_send(741, self.ui.input_spoolsidefreearea.value())
        time.sleep(delaybetweensending)        
        self.fun_send(750, self.ui.input_spoolsiderpm.value())
        time.sleep(delaybetweensending)        
        self.fun_send(780, self.ui.input_tfsidewayrpm.value())
        time.sleep(delaybetweensending)        
        self.fun_send(790, self.ui.input_tfsidewaydistance.value())
        time.sleep(delaybetweensending)     
        self.fun_send(875, self.ui.input_autofixsteps.value())
        time.sleep(delaybetweensending)
        self.fun_send(885, self.ui.input_spoolsidewaycorrection.value())
        time.sleep(delaybetweensending)
        
 
        
        #Set progressbar maximum value to match the one sent to arduino
        self.ui.progressBar.setMaximum(self.ui.input_spoolsidefreearea.value())
        self.sidetepmax=self.ui.input_spoolsidefreearea.value()
        
        #Set parameters_sent variable true. Possible to check if params have been sent
        self.parameters_sent = True
        self.ui.btn_sendparameters.setStyleSheet('QPushButton')
        
  
## INFORM USER WHEN PARAMETERS HAVE CHANGED
    def parameters_changed(self):
        if not self.compareConfig():
            self.ui.btn_sendparameters.setStyleSheet('QPushButton {background-color: lime}')
#            self.parameters_sent = False
            self.parameters_same = False
#            print "parameters_same = " +str(self.parameters_same)
            
        elif self.compareConfig():
            self.ui.btn_sendparameters.setStyleSheet('QPushButton')
            self.parameters_same = True
#            print "parameters_same = " +str(self.parameters_same)
        

## SAVE CONFIGURATION TO FILE
    def saveconfig(self):
        tf.msg.write("Saving config to " + self.configfilename)
        cfgfile = open(self.configfilename,'w')
        if ((self.Config.has_section('Parameters'))==0):
            self.Config.add_section('Parameters')
        self.Config.set('Parameters','stepdelay',self.ui.input_stepdelay.value())
        self.Config.set('Parameters','clamp1open',self.ui.input_clamp1open.value())
        self.Config.set('Parameters','clamp1close',self.ui.input_clamp1close.value())
        self.Config.set('Parameters','clamp2close',self.ui.input_clamp2close.value())
        self.Config.set('Parameters','clamp2open',self.ui.input_clamp2open.value()) 
        self.Config.set('Parameters','tensionoff',self.ui.input_tensionoff.value()) 
        self.Config.set('Parameters','tensionon',self.ui.input_tensionon.value())
        self.Config.set('Parameters','flatteneron',self.ui.input_flatteneron.value()) 
        self.Config.set('Parameters','flatteneroff',self.ui.input_flatteneroff.value())
        self.Config.set('Parameters','flattenerarmin',self.ui.input_flattenerarmin.value()) 
        self.Config.set('Parameters','flattenerarmout',self.ui.input_flattenerarmout.value())
        self.Config.set('Parameters','pinon',self.ui.input_pinon.value())
        self.Config.set('Parameters','pinoff',self.ui.input_pinoff.value())
        self.Config.set('Parameters','spoolsteps',self.ui.input_spoolsteps.value())
        self.Config.set('Parameters','stepperrpm',self.ui.input_stepperrpm.value())
        self.Config.set('Parameters','spoolsidesteps',self.ui.input_spoolsidesteps.value())
        self.Config.set('Parameters','spoolsidefreearea',self.ui.input_spoolsidefreearea.value())        
        self.Config.set('Parameters','spoolsiderpm',self.ui.input_spoolsiderpm.value())
        self.Config.set('Parameters','tfsidewaydistance',self.ui.input_tfsidewaydistance.value())
        self.Config.set('Parameters','tfsidewayrpm',self.ui.input_tfsidewayrpm.value())
        self.Config.set('Parameters','autofixsteps',self.ui.input_autofixsteps.value())		
        self.Config.set('Parameters','spoolsidewaycorrection',self.ui.input_spoolsidewaycorrection.value())
        self.Config.write(cfgfile)
        cfgfile.close()
        self.Config.read(self.configfilename) #Needed because of bug in python 2.6
        tf.msg.write("OK")
        
  

#SAVE CONFIG TO FILE SELECTED BY USED
#TODO: THIS CAN BE COMBINED WITH saveconfig (ONLY 2 first lines differ..)
    def saveconfigas(self):
        self.configfilename = str(QtGui.QFileDialog.getSaveFileName(self, 'Save as..'))
        self.ui.lbl_filename.setText(self.configfilename) #Show new filename
        tf.msg.write("Saving config to " + self.configfilename)
        cfgfile = open(self.configfilename,'w')
        if ((self.Config.has_section('Parameters'))==0):
            self.Config.add_section('Parameters')
        self.Config.set('Parameters','stepdelay',self.ui.input_stepdelay.value())
        self.Config.set('Parameters','clamp1open',self.ui.input_clamp1open.value())
        self.Config.set('Parameters','clamp1close',self.ui.input_clamp1close.value())
        self.Config.set('Parameters','clamp2close',self.ui.input_clamp2close.value())
        self.Config.set('Parameters','clamp2open',self.ui.input_clamp2open.value()) 
        self.Config.set('Parameters','tensionoff',self.ui.input_tensionoff.value()) 
        self.Config.set('Parameters','tensionon',self.ui.input_tensionon.value())
        self.Config.set('Parameters','flatteneron',self.ui.input_flatteneron.value()) 
        self.Config.set('Parameters','flatteneroff',self.ui.input_flatteneroff.value())
        self.Config.set('Parameters','flattenerarmin',self.ui.input_flattenerarmin.value()) 
        self.Config.set('Parameters','flattenerarmout',self.ui.input_flattenerarmout.value())
        self.Config.set('Parameters','pinon',self.ui.input_pinon.value())
        self.Config.set('Parameters','pinoff',self.ui.input_pinoff.value())
        self.Config.set('Parameters','spoolsteps',self.ui.input_spoolsteps.value())
        self.Config.set('Parameters','stepperrpm',self.ui.input_stepperrpm.value())
        self.Config.set('Parameters','spoolsidesteps',self.ui.input_spoolsidesteps.value())
        self.Config.set('Parameters','spoolsidefreearea',self.ui.input_spoolsidefreearea.value())                
        self.Config.set('Parameters','spoolsiderpm',self.ui.input_spoolsiderpm.value())
        self.Config.set('Parameters','tfsidewaydistance',self.ui.input_tfsidewaydistance.value())
        self.Config.set('Parameters','tfsidewayrpm',self.ui.input_tfsidewayrpm.value())
        self.Config.set('Parameters','autofixsteps',self.ui.input_autofixsteps.value())		
        self.Config.set('Parameters','spoolsidewaycorrection',self.ui.input_spoolsidewaycorrection.value())
        self.Config.write(cfgfile)
        cfgfile.close()
        self.Config.read(self.configfilename) #Needed because of bug in python 2.6
        tf.msg.write("OK")


## LOADING CONFIGURATION FROM A FILE
    def openconfig(self):
        #use dialog to get filename
        self.configfilename = str(QtGui.QFileDialog.getOpenFileName(self, 'Open file'))

        #Messages
        self.ui.lbl_filename.setText(self.configfilename)
        tf.msg.write("Loading config from " + self.configfilename)
        
        #load config
        try:
            self.Config.read(self.configfilename)
            self.ui.input_stepdelay.setValue(self.Config.getint('Parameters','stepdelay'))
            self.ui.input_clamp1open.setValue(self.Config.getint('Parameters','clamp1open'))
            self.ui.input_clamp1close.setValue(self.Config.getint('Parameters','clamp1close'))
            self.ui.input_clamp2close.setValue(self.Config.getint('Parameters','clamp2close'))
            self.ui.input_clamp2open.setValue(self.Config.getint('Parameters','clamp2open'))
            self.ui.input_tensionoff.setValue(self.Config.getint('Parameters','tensionoff'))
            self.ui.input_tensionon.setValue(self.Config.getint('Parameters','tensionon'))
            self.ui.input_flatteneron.setValue(self.Config.getint('Parameters','flatteneron'))
            self.ui.input_flatteneroff.setValue(self.Config.getint('Parameters','flatteneroff'))
            self.ui.input_flattenerarmin.setValue(self.Config.getint('Parameters','flattenerarmin'))
            self.ui.input_flattenerarmout.setValue(self.Config.getint('Parameters','flattenerarmout'))
            self.ui.input_pinon.setValue(self.Config.getint('Parameters','pinon'))
            self.ui.input_pinoff.setValue(self.Config.getint('Parameters','pinoff'))
            self.ui.input_spoolsteps.setValue(self.Config.getint('Parameters','spoolsteps'))
            self.ui.input_stepperrpm.setValue(self.Config.getint('Parameters','stepperrpm'))
            self.ui.input_spoolsidesteps.setValue(self.Config.getint('Parameters','spoolsidesteps'))
            self.ui.input_spoolsidefreearea.setValue(self.Config.getint('Parameters','spoolsidefreearea'))
            self.ui.input_spoolsiderpm.setValue(self.Config.getint('Parameters','spoolsiderpm'))
            self.ui.input_tfsidewayrpm.setValue(self.Config.getint('Parameters','tfsidewayrpm'))
            self.ui.input_tfsidewaydistance.setValue(self.Config.getint('Parameters','tfsidewaydistance'))
            self.ui.input_autofixsteps.setValue(self.Config.getint('Parameters','autofixsteps'))
            self.ui.input_spoolsidewaycorrection.setValue(self.Config.getint('Parameters','spoolsidewaycorrection'))
            
            tf.msg.write("OK")
        except (ConfigParser.MissingSectionHeaderError):
                self.write("Incorrect or corrupt config file!")
    

### COMPARE CURRENT WORKING CONFIG TO LOADED ONE    
    def compareConfig(self):
        
        oldconfig = []
        oldconfig.append(self.Config.getint('Parameters','stepdelay'))
        oldconfig.append(self.Config.getint('Parameters','clamp1open'))
        oldconfig.append(self.Config.getint('Parameters','clamp1close'))
        oldconfig.append(self.Config.getint('Parameters','clamp2close'))
        oldconfig.append(self.Config.getint('Parameters','clamp2open'))
        oldconfig.append(self.Config.getint('Parameters','tensionoff'))
        oldconfig.append(self.Config.getint('Parameters','tensionon'))
        oldconfig.append(self.Config.getint('Parameters','flatteneron'))
        oldconfig.append(self.Config.getint('Parameters','flatteneroff'))
        oldconfig.append(self.Config.getint('Parameters','flattenerarmin'))
        oldconfig.append(self.Config.getint('Parameters','flattenerarmout'))
        oldconfig.append(self.Config.getint('Parameters','pinon'))
        oldconfig.append(self.Config.getint('Parameters','pinoff'))
        oldconfig.append(self.Config.getint('Parameters','spoolsteps'))
        oldconfig.append(self.Config.getint('Parameters','stepperrpm'))
        oldconfig.append(self.Config.getint('Parameters','spoolsidesteps'))
        oldconfig.append(self.Config.getint('Parameters','spoolsidefreearea'))
        oldconfig.append(self.Config.getint('Parameters','spoolsiderpm'))
        oldconfig.append(self.Config.getint('Parameters','tfsidewayrpm'))
        oldconfig.append(self.Config.getint('Parameters','tfsidewaydistance'))
        oldconfig.append(self.Config.getint('Parameters','autofixsteps'))
        oldconfig.append(self.Config.getint('Parameters','spoolsidewaycorrection'))


        newconfig = []
        newconfig.append(self.ui.input_stepdelay.value())
        newconfig.append(self.ui.input_clamp1open.value())
        newconfig.append(self.ui.input_clamp1close.value())
        newconfig.append(self.ui.input_clamp2close.value())
        newconfig.append(self.ui.input_clamp2open.value())
        newconfig.append(self.ui.input_tensionoff.value())
        newconfig.append(self.ui.input_tensionon.value())
        newconfig.append(self.ui.input_flatteneron.value())
        newconfig.append(self.ui.input_flatteneroff.value())
        newconfig.append(self.ui.input_flattenerarmin.value())
        newconfig.append(self.ui.input_flattenerarmout.value())
        newconfig.append(self.ui.input_pinon.value())
        newconfig.append(self.ui.input_pinoff.value())
        newconfig.append(self.ui.input_spoolsteps.value())
        newconfig.append(self.ui.input_stepperrpm.value())
        newconfig.append(self.ui.input_spoolsidesteps.value())
        newconfig.append(self.ui.input_spoolsidefreearea.value())
        newconfig.append(self.ui.input_spoolsiderpm.value())
        newconfig.append(self.ui.input_tfsidewayrpm.value())
        newconfig.append(self.ui.input_tfsidewaydistance.value())
        newconfig.append(self.ui.input_autofixsteps.value())
        newconfig.append(self.ui.input_spoolsidewaycorrection.value())


        if (newconfig == oldconfig):
            return True
        else:
            return False
        
    def showlog(self):
        self.editor = Editor()
        self.editor.show()
        self.editor.setText(open("tf.log").read())
        
    def showCyclePosition(self, position):
        if position == 1:
            self.endofcycle = False
            self.ui.cycle_pos1.setChecked(True)
            self.ui.cycle_pos2.setChecked(False)
            self.ui.cycle_pos3.setChecked(False)
            self.ui.cycle_pos4.setChecked(False)
            self.ui.cycle_pos5.setChecked(False)
        elif position == 2:
            self.ui.cycle_pos1.setChecked(False)
            self.ui.cycle_pos2.setChecked(True)
            self.ui.cycle_pos3.setChecked(False)
            self.ui.cycle_pos4.setChecked(False)
            self.ui.cycle_pos5.setChecked(False)
        if position == 3:
            self.ui.cycle_pos1.setChecked(False)
            self.ui.cycle_pos2.setChecked(False)
            self.ui.cycle_pos3.setChecked(True)
            self.ui.cycle_pos4.setChecked(False)
            self.ui.cycle_pos5.setChecked(False)
        if position == 4:
            self.ui.cycle_pos1.setChecked(False)
            self.ui.cycle_pos2.setChecked(False)
            self.ui.cycle_pos3.setChecked(False)
            self.ui.cycle_pos4.setChecked(True)
            self.ui.cycle_pos5.setChecked(False)
        if position == 5:
            self.ui.cycle_pos1.setChecked(False)
            self.ui.cycle_pos2.setChecked(False)
            self.ui.cycle_pos3.setChecked(False)
            self.ui.cycle_pos4.setChecked(False)
            self.ui.cycle_pos5.setChecked(True)
            self.endofcycle = True
            if self.inspectionmalfuction:
                self.write("Stopping autorun on cycle start.")
                self.fun_autorun_stop()
                time.sleep(2)
                self.fun_attachservos(0)
            
            
    
    def closeEvent(self, event):
        
        if (self.connection == 1): # Check if TF still connected
            disconnect_msg = "TF still connected! Disconnect first?"
            reply1 = QtGui.QMessageBox.question(self, 'Message', 
                             disconnect_msg, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
        
            if reply1 == QtGui.QMessageBox.Yes:
                self.fun_close_serial()
            else:
                pass
                
        res = self.compareConfig() # See if changes have been made to TF parameters
        if not res:
            quit_msg = "Tether Factory parameters have been changed. \n Are you sure you want to exit the program?"
            reply2 = QtGui.QMessageBox.question(self, 'Message', 
                             quit_msg, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No, QtGui.QMessageBox.Save)
        
            if reply2 == QtGui.QMessageBox.Yes:
                self.endcommand()
                #event.accept()
            elif reply2 == QtGui.QMessageBox.Save:
                self.saveconfigas()
            else:
                event.ignore()
        
        else:
            self.endcommand()
    
### FIND OUT IF THERE IS A INTERNET CONNECTION    
    def networkAvailable(self):
        try:
            response=urllib2.urlopen('http://74.125.113.99',timeout=1)
            return True
        except urllib2.URLError as err: pass
        return False
    
### RETURN CURRENT STATUS OF TF IN HTML
    def currentStatus(self):
        statusstring ="Welcome to Tether Factory web interface 0.1!<br><br>The TF software is running.. Current status: <br><br>"
        if self.connection:
            statusstring += "Master arduino: CONNECTED<br>"
            if self.stoppedonbadbond:
            	statusstring += "<b><br>STOPPED ON A FAILED BOND</b> on "+ self.stoppedonbadbondtime +"<br><br>"
            elif self.wedgedown:
                statusstring += "<b><br>WEDGE DOWN IN WRONG CYCLE</b> at "+ self.stoppedonwedgedowntime +"<br><br>"
            elif self.inspectionmalfuction:
                statusstring += "<b><br>INSPECTION MALFUNCTION</b> at "+ self.inspectionmalfunctiontime +"<br><br>"
            statusstring += "Autorun mode: <b>"+ str(self.autorunenabled) +"</b><br>"
            statusstring += "Bonds: "+ str(int(self.ui.lcdNumber.value())) +"<br>"
            statusstring += "Lenght: "+ str(float(self.ui.tetherLengthLcd.value())) +"<br>"
            statusstring += "# of failed bonds: "+ str(int(self.ui.lcd_bondfailcount.value())) +"<br>"
            statusstring += "# autofixed bonds: "+ str(self.autofixedbonds) +"<br>"
            
        else:
            statusstring += "Master arduino: NOT CONNECTED<br>"
        
        return statusstring                

### Handle the messages currently in the queue(s) (if any).    
    def processIncoming(self):
        while self.commandqueue.qsize()>0:
            try:
                command,data = self.commandqueue.get(0)
                #print command,data
                if (command < 100):
                    self.write("Corrupted command received. Ignoring, logged.")
                    tf.logger.warning("Corrupted transmission! # of commands:" + str(command) + ", Received: !" +str(data))
                elif (command == 910):
                    self.showCyclePosition(data)
                elif (command == 930):
                    tf.logger.info("BOND! Command:" + str(command) + ", Data:" +str(data))
                    self.fun_changeLcdCount(1, data)
                    if self.autofix:
                        self.fun_autofix(2)
                elif (command == 940):
                    self.fun_showSpoolSidewayMovement(data)
                    tf.logger.info("Spool sideways movement! Command:" + str(command) + ", Data:" +str(data))
                elif (command == 950):
                    self.fun_showWire(data)
                    tf.logger.info("Wire position! Command:" + str(command) + ", Data:" +str(data))
                elif (command == 970):
                    self.fun_showTetherLength(data)
                    tf.logger.info("Tether length! Command:" + str(command) + ", Data:" +str(data))
                elif (command == 980):
                    tf.logger.warning("BAD BOND! Command:" + str(command) + ", Data:" +str(data))
                    self.fun_badBondStop(data)
                elif (command == 981):
                    self.write("Bad bond count: " +str(data))
                    self.fun_changeLcdCount(2, data)
                elif (command == 982):
                    self.fun_autofix(1)
                    tf.logger.warning("Autofix succeeded!")
                elif (command == 985):
                    self.fun_wedgeDownStop(data)
                elif (command == 960):
                    if (data == 1):
                        self.write("Connected to a master Arduino..OK")
                        tf.logger.info("Connected to a MASTER Arduino. Command:" + str(command) + ", Data:" +str(data))
                    elif (data == 2):
                        self.write("Connected to a slave Arduino..OK")
                        tf.logger.info("Connected to a SLAVE Arduino. Command:" + str(command) + ", Data:" +str(data))
                    else:
                        pass
                elif (command == 990):
                    if (data == 1):
                        self.write("Servos attached!")
                        tf.logger.info("Servos attached!")
                    elif (data == 2):
                        self.write("Servos detached!")
                        tf.logger.info("Servos detached!")
                elif (command == 995):
                    if self.useinspection:
                        self.fun_checkinspection(data)
                

                elif (command == 999):
                    self.write ("Received: Command: " +str(command) + " Data: " +str(data))
                    
            except Queue.Empty:
                pass
            
class Server(object):
    def __init__(self, ui):
        self.ui = ui
        
    def index(self):
    	if self.ui.autorunenabled:
    		html = "<html><head><title>Tetherfactory WebUI</title></head><body>" + self.ui.currentStatus() + '<br><a href="download">TF Log</a>' + '<br><a href="photo">Latest photo</a>' +""" 
    		<br><br> <b>STOP AUTORUN</b><br><form action="stopprocess" method="POST"><input name="var" value="stop" type="submit"></form>	</body></html>"""
    		return html
    	else:
			html = "<html><head><title>Tetherfactory WebUI</title></head><body>" + self.ui.currentStatus() + '<br><a href="download">TF Log</a>' + '<br><a href="photo">Latest photo</a></body></html>'
			return html
    index.exposed = True
        
    def download(self):
        return cherrypy.lib.static.serve_download(os.path.abspath(os.path.dirname(__file__)) + '/tf.log')
    download.exposed = True
    
    def photo(self):
    	return cherrypy.lib.static.serve_download(os.path.abspath(os.path.dirname(__file__)) + '/snap.jpg')
    photo.exposed = True
    
    def stopprocess(self, var=None):
    	print var
    	if var == 'stop':
			self.ui.fun_autorun_stop()
    	return "Autorun stopped!"
    stopprocess.exposed = True

	
		
class ThreadedClient:
    """
    Launch the main part of the GUI and the serial reader thread.
    """
    def __init__(self):
        # Create the queue for commands
        self.commandqueue = Queue.Queue()
        self.Config = ConfigParser.ConfigParser()
        # Set up the GUI
        self.ui=GUI(self.commandqueue, self.endApplication, self.Config)
        global tf
        tf=tetherfactory.TetherFactory(self.ui)
        try: 
            self.ui.openconfig()
        except (ConfigParser.NoSectionError):
            tf.msg.write("Config file not loaded!")
		

        # A timer to periodically call periodicCall()
        self.timer = QtCore.QTimer()
        QtCore.QObject.connect(self.timer,
                           QtCore.SIGNAL("timeout()"),
                           self.periodicCall)
        # Start the timer, calls are made every 0.05 sec
        self.timer.start(5)

        # Set up the thread to do asynchronous I/O
        self.running = 1
        self.thread1 = threading.Thread(target=self.workerThread1)
        self.thread1.setDaemon(True) ## Set the tread daemonic, so it always gets killed on gui exit.
        self.thread1.start()

        # Start webserver
        cherrypy.config.update({'server.socket_port': 8080, 'server.socket_host' : '0.0.0.0'})
        userpassdict = {'TF' : 'TF'}
        stoppassdict = {'TFstop' : 'TFstop'}
        checkpassword = cherrypy.lib.auth_basic.checkpassword_dict(userpassdict)
        checkpasswordforstop = cherrypy.lib.auth_basic.checkpassword_dict(stoppassdict)
        cherrypy.tree.mount(Server(self.ui), '/', config={
                                           '/': { 
#                                           'tools.staticdir.on': True,
#                                           'tools.staticdir.dir': os.path.abspath(os.path.dirname(__file__)) + '/photos',
                                           'tools.auth_basic.on': True,
                                           'tools.auth_basic.realm': 'Password for Tether Factory web interface',
                                           'tools.auth_basic.checkpassword': checkpassword,
                                           },
                                           '/stopprocess': {
											'tools.auth_basic.on': True,
										   'tools.auth_basic.realm': 'Additional password required to stop process!',
										   'tools.auth_basic.checkpassword': checkpasswordforstop,
					  						},
                                           })

        cherrypy.engine.start()

        
    #Check every 50 ms if there is something new in the queue.
    def periodicCall(self):
        self.ui.processIncoming()
        if not self.running:
            app.quit()
        

    #End the thread when closing the GUI        
    def endApplication(self):
        tf.logger.warning("Thread closed!")
        print "cherry exit.."
        cherrypy.engine.exit()
        print "..done."
        self.running = 0
 
    #Method to check for a serial connection flag in the GUI    
    def isConnected(self):
        if self.ui.connection==1:
            return 1
        else: 
            return 0



    #The Serial reader thread. Receive commands from the serial.
    #For now pickybagging on tf.receivecommand() method.
    #Could be done more efficiently eg. with select()
    def workerThread1(self):
        
        while self.running:
            if self.isConnected():   
                #print "connected"
                command,data = tf.receivecommand()
                if command > 0:
                    self.commandqueue.put((command,data))
#                    print (command,data)
#                    time.sleep(0.05)
                else:
                    time.sleep(0.005)        # Sleep if no commands in serial buffer
            else:
                #print "not connected"
                time.sleep(0.05)       # Sleep if arduino not connected

              

if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    client = ThreadedClient()
    sys.exit(app.exec_())
