/*
  E-Sail tether factory control software
 
 Copyright 2011 Risto H. Kurppa risto at kurppa . fi under the GPLv3+ License
           2012 Timo Rauhala trauhala at gmail . com under the GPLv3+ License

 
 GPL3 LICENSE
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 */

// INCLUDES
#include <Servo.h> 
#include <Stepper.h>



// ************************
//  AUTOMATIZATION PARAMETERS, INITIAL VALUES
// ************************
boolean sendstatus = false;  //Send message about cycle status to python

boolean automaticmode=false;

boolean initmode = 0; //Check if initmode button pressed = start autorun-loop from beginig

//TETHER FACTORY PARAMETERS

int max_automatizationdelay=2000; //ms between steps in automatic mode  
int stepdelay=300; //delay between steps

int stepper_speed=5; //rpm, speed of stepper
int stepper_steps=35; // how many steps is the wire spooled between bonds


int stepper2_speed=40; //how fast is the transl. stage moved
int stepper2_sidesteps=50; //how many steps the transl.stage is moved
int stepper2_freearea=21000; //fHow big area is allowed for spool sideway movement. Starting from one side.
int stepper2_position=0; //Where is the spool now? Changes when the spool moves.
int stepper2_direction=1; //Direction where stepper2 is moving

int stepper3_speed=1; //how fast is the whole TF moved
int stepper3_distance=10; //How many steps is the distance between bond spots

int clamp1_open=90;
int clamp1_close=50;

int tension_off=40;
int tension_on=29;

int flattener_off=102;
int flattener_on=75;

int flattenerarm_in=90;
int flattenerarm_out=15;

int pin_off=133;
int pin_on=126;

int clamp2_open=20;
int clamp2_close=150;

int bonderclamp_opened_pwm=170;

int autofixsteps = 5;

boolean tfsidewaycorrectionbool = false;
int tfsidewaycorrectioncycles = 35;  //How many cycles between corrections on the wire select
int tfsidewaycorrection = 1; // Correct the sideways movement by this ammount on the center to left wire movement. NOT WORKING ON MANUAL MODE!
int spoolsidewaycorrection = 38; //Correct spool position by this ammount each cycle backwards on Sergiy style spooling

// ************************
//  PINS & OTHER CONSTANT VARIABLES 
// ************************

//STEPPER step counts per one round
const int STEPS = 192;
const int STEPS2 = 192;  //TODO: Change to 200 when params can be reset
const int STEPS3 = 200; 

//Precompiler fills in __DATE__ and  __TIME__
const char compiledate[] = __DATE__; 
const char compiletime[] = __TIME__; 

//DIGITAL OUTPUT 
const int pin_servo1=6;// - clamp1
const int pin_servo2=5;// - tension
const int pin_servo3=4;// - flattener
const int pin_servo4=3;// - clamp2
const int pin_servo5=2;// - pin
const int pin_servo6=10; // - flattenerarm

const int pin_stepper2_1=39;
const int pin_stepper2_2=45;
const int pin_stepper2_3=41;
const int pin_stepper2_4=43;

const int pin_stepper3_1=47;
const int pin_stepper3_2=51;
const int pin_stepper3_3=53;
const int pin_stepper3_4=49;

//const int pin_stepper1=39; //
//const int pin_stepper2=45; //  Not in use! Stepper1 = Spooling motor
//const int pin_stepper3=41; //  on slave arduino! 
//const int pin_stepper4=43; //


//SOLENOIDS.. TO CONTROL BONDER SOLENOID..?
//const int pin_solenoid=7;  // FAKE!!
const int pin_bonderclamp=7;
const int pin_bonderswitch=35;

//ARDUINO LED.. NOT REALLY USED MUCH..
const int pin_led=13;

//Pin to connect with camera 
const int camerapin=A8;



// ************************
//  RUNTIME VARIABLES 
// ************************

//STATUS VARIABLES
int solenoid_on=0; // is flattener tilt solenoid on or off (off = flattener away from flattening position)
int clamp_on=0; // Is bonder clamp on or off
unsigned long stepperstepcounter=0; // Counter to count the steps the spooling stepper has walked
unsigned long bonds=0; // Counter for number of bonds
unsigned int command[2];
unsigned int wirecounter=2; //This knows/records on what upper wire position we are, 1, 2 or 3. 0=uninitialized
unsigned int rank = 1; // 1 == Master, 2 == Slave
unsigned int spooling = 0; // 0 == NOT currently spooling on slave, 2 == Currently spooling on slave
int wselect = 0; // 0 == NOT currently wireselect on slave, 1 == Currently wireselect on slave
unsigned int badbondcount = 0;
unsigned int wedgeorlightdowncount = 0;
unsigned int camerapinstate = 0; //Variable for camera pin state
int cycleposition = 0;
int inspectiononline = 0; // Hold status of the inspection system during semi & automatic mode
unsigned int spoolsidewaymovements = 0;
unsigned int tfsidewaymovements = 0;

// ***************************
// Initializing classes
// ***************************
Servo servo_clamp1;  // create servo object to control a servo 
Servo servo_tension;  // create servo object to control a servo 
Servo servo_flattener;  // create servo object to control a servo 
Servo servo_flattenerarm;  // create servo object to control a servo 
Servo servo_clamp2;  // create servo object to control a servo 
Servo servo_pin;  // create servo object to control a servo 

// Stepper stepper(STEPS, pin_stepper1, pin_stepper2, pin_stepper3, pin_stepper4);    // Spooling motor on slave!
Stepper stepper2(STEPS2, pin_stepper2_1, pin_stepper2_2, pin_stepper2_3, pin_stepper2_4);  
Stepper stepper3(STEPS3, pin_stepper3_1, pin_stepper3_2, pin_stepper3_3, pin_stepper3_4);  


void setup() {

  //SERIAL
  Serial.begin(115200);
  Serial1.begin(115200);
  //SERVOS now attached/detached in method connectservos(boolean servoconnection)
//  servo_clamp1.attach(pin_servo1);
//  servo_tension.attach(pin_servo2);
//  servo_flattener.attach(pin_servo3);
//  servo_clamp2.attach(pin_servo4);
//  servo_pin.attach(pin_servo5);
//  servo_flattenerarm.attach(pin_servo6);

  //STEPPER
  //stepper.setSpeed(stepper_speed);  // Spooling motor on slave!
  stepper2.setSpeed(stepper2_speed);
  stepper3.setSpeed(stepper3_speed);
    
  //LED PIN
  pinMode(pin_led, OUTPUT);    

  //SOLENOID
//  pinMode(pin_solenoid, OUTPUT);
//  digitalWrite(pin_solenoid,LOW);

  //BONDER CLAMP CONTROL
  pinMode(pin_bonderclamp, OUTPUT);
  analogWrite(pin_bonderclamp, 0);

  //BONDER SWITCH
  pinMode(pin_bonderswitch, OUTPUT);
  
  //CAMERA CONTROL PIN
  pinMode(A8, INPUT_PULLUP);


//SET BONDER BUTTONS SAFE INITIAL POSITIONS
  
  // Release bonder switch
  digitalWrite(pin_bonderswitch, LOW);
  delay(100); 

  //Open bonder clamp
  analogWrite(pin_bonderclamp, bonderclamp_opened_pwm);    
  delay(100); 
  
//  //flattener up
//  servo_flattener.write(flattener_off);
//  delay(100); 
//  
//  // Remove flattener arm
//  servo_flattenerarm.write(flattenerarm_out);
//  delay(100); 
//  
//  //remove pin
//  servo_pin.write(pin_off);
//  delay(100); 
//  
//  //raise tension arm
//  servo_tension.write(tension_off);
//  delay(100); 
//  
//  //open clamp2
//  servo_clamp2.write(clamp2_open);  
//  delay(100); 
//  
//  //open clamp1
//  servo_clamp1.write(clamp1_open);
//  delay(100); 
  
//  Serial.println("Ready");
}





void loop() {
  sendcycleposition(1);
  waitforstep(command);
  if (initmode == 1) {initmode = 0; return; } //Check if initmode has been enabled
  processstep(200); //Close clamp 1
  processstep(210); //Close clamp 2
  processstep(220); //Lower tension arm
  processstep(230); //Show flattener
  sendcycleposition(2);
  waitforstep(command);
  if (initmode == 1) {initmode = 0; return; } //Check if initmode has been enabled
  processstep(240); //FLATTEN!
  processstep(250); //Raise flattener
  processstep(260); //Remove flattener
  processstep(270), //Insert pin
  processstep(280); //"Close bonder clamp
              processstep(300); //Lower w. for upper flat.
              sendcycleposition(3);
              waitforstep(command);
              if (initmode == 1) {initmode = 0; return; } //Check if initmode has been enabled
              delay(700); 
              processstep(310); //Flatten upper wire
              delay(350);
              checkforinspectionsystem();
              delay(350);
              processstep(340); //Lower to 2nd b. search height
  processstep(350); //Open bonder clamp
              sendcycleposition(4);
              waitforstep(command);
              if (initmode == 1) {initmode = 0; return; } //Check if initmode has been enabled
              delay(700);
              processstep(360); //BOND!
  delay(900); //Delay to give camera enough time to react
  processstep(160); // Check for bad bonds!
  sendcycleposition(5);
  waitforstep(command);
  if (initmode == 1) {initmode = 0; return; } //Check if initmode has been enabled
  processstep(380); //Remove pin
  processstep(390); //Remove tension
  processstep(400); //Open Clamp 1
  processstep(410); //Open Clamp 2
  delay(stepdelay);
  connectservos(false);
  processstep(420); //Spooling
  processstep(430); //Move spool sideways
  processstep(795); //Move to next wire in loop of (1,2,3)
  delay(stepdelay);
  connectservos(true);
  delay(stepdelay);
  processstep(162);  //Check if wedge or lights down
}

void initposition() { // TODO: Adjust indicators correctly
  initmode = 1; //Set boolean initmode true, check condition in loop()
  processstep(350); //Open bonder clamp
  processstep(361); //Release bonder buttonBOND!, do not add to bondcount
  processstep(380); //Remove pin
  processstep(250); //Raise flattener
  processstep(260); //Flattener arm out
  processstep(390); //Remove tension
  processstep(400); //Open Clamp 1
  processstep(410); //Open Clamp 2

}

void processstep(int step){
  delay(stepdelay);
  switch (step){
  case 110: //TEST, returns string over serial
    Serial.println("Magic");
    break;

  case 111: //returns master(0)/slave(1) string over serial
    writeoutput(960,rank); // Use command 960 for telling UI where we are connected.
    break;
    
  case 112: //polls slave for current wirenumber
    wselect = 1; 
    writeslaveoutput(112,1);
    break;

  case 113: //returns current wirenumber
    wselect = 0;
    writeoutput(950, command[1]);
    break;
    
  case 120: //TEST, retursn !92048888 over serial (=8888 completed bonds!)
    writeoutput(920,8888);
    break;
    
  case 130: //Activate AUTO RUN mode
    automaticmode=true;
    break;
    
  case 140: //Disable AUTO RUN mode
    automaticmode=false;	
    break;
    
  case 150: //INITIALIZE the TF = release wire
    initposition();
    break;
    
  case 160:
    camerapinstate = analogRead(camerapin);
    Serial.println(camerapinstate);
    if (camerapinstate < 750) {
      badbondcount++;
      writeoutput(980, badbondcount);
      automaticmode=false;
    }
    else {
      break;
    }
    break;
  
    
  case 162:
    camerapinstate = analogRead(camerapin);
    if (camerapinstate < 750) {
      wedgeorlightdowncount++;
      writeoutput(985, wedgeorlightdowncount);
      automaticmode=false;
    }
    else {
      break;
    }
    break;    

    
  case 165:
    writeoutput(981, badbondcount);
    break;
    
  case 170: // Connection / Reader tester
    while (true) {
      Serial.println("!999512345");
      if (command[1] > 0) {
        delay(command[1]);
      }
      else {
        delay(10);
      }
      if (Serial.available() > 0) {
        break;
      }
    }
    break;
    
  case 180: // Attach/detach servos!
    if (command[1] == 1) {
      connectservos(true);
      writeoutput(990,1);
    }
    else if (command[1] == 2) {
      connectservos(false);
      writeoutput(990,2);
    }
    break;
  
  case 200: //Close clamp 1
    //close clamp1
    servo_clamp1.write(clamp1_close);
    statusmsg('1');
    break;
  case 210: //Close clamp 2
    //close clamp2
    servo_clamp2.write(clamp2_close);
    statusmsg('2');
    break;
  case 220: //Lower tension arm
    //lower tension arm
    servo_tension.write(tension_on);
    statusmsg('3');
    break;
  case 230: //Flattener arm in
    //show flattener
    servo_flattenerarm.write(flattenerarm_in);
    statusmsg('3');
    break;
  case 240: //FLATTEN!
    //flattener down
    servo_flattener.write(flattener_on);
    statusmsg('5');
    break;
  case 250: //Raise flattener
    //flattener up
    servo_flattener.write(flattener_off);
    statusmsg('6');
    break;
  case 260: //Flattener arm out
    //take flattener away
    servo_flattenerarm.write(flattenerarm_out);
    statusmsg('7');
    break;
  case 270: //Insert pin
    //show pin
    servo_pin.write(pin_on);
    statusmsg('8');
    break;
  case 280: //"Close bonder clamp
    //close bonder clamp
    analogWrite(pin_bonderclamp, 0);
    statusmsg('9');
    break;
  case 290: //Move to upper flat. pos.
    //move transl. stage for lower flattening
    statusmsg('10');
    break;
  case 300: //Lower w. for upper flat.
    //bonder arm down
    digitalWrite(pin_bonderswitch,HIGH);
    statusmsg('11');
    break;
  case 310: //Flatten upper wire
    //BOND1 - flattening
    digitalWrite(pin_bonderswitch,LOW);
    statusmsg('12');
    break;
  case 320: //Lift w. after upper flat
    //bonder arm up
    //message="Lift w. after upper flat.";
    //waitforbutton(message, command);
    statusmsg('13');
    break;
  case 330: //Move to bond position
    //move transl. stage back to bond position
    statusmsg('14');
    break;
  case 340: //Lower to 2nd b. search height
    //bonder wedge down
    digitalWrite(pin_bonderswitch,HIGH);
    statusmsg('15');
    break;
  case 350: //Open bonder clamp
    //open bonder clamp
    analogWrite(pin_bonderclamp, bonderclamp_opened_pwm);
    statusmsg('16');
    break;
  case 360: //BOND!
    //BOND2
    digitalWrite(pin_bonderswitch,LOW);
    bonds++;
    statusmsg('17');
    writeoutput(930,bonds); //always send command 930 after a BOND
    break;
  case 361: //Release bonder buttonBOND!
    digitalWrite(pin_bonderswitch,LOW);
    break;
  case 370: //Lift wedge after bonding
    //bonder wedge up
    //message="Lift wedge after bonding";
    //waitforbutton(message, command);
    statusmsg('18');
    break;
  case 380: //Remove pin
    //remove pin
    servo_pin.write(pin_off);
    statusmsg('19');
    break;
  case 390: //Remove tension
    //raise tension arm
    servo_tension.write(tension_off);
    break;
  case 400: //Open Clamp 1
    //open clamp1
    servo_clamp1.write(clamp1_open);
    break;
  case 410: //Open Clamp 2
    //open clamp2
    servo_clamp2.write(clamp2_open);  
    break;
  case 420: //Spool
    //spool
    writeslaveoutput(420,stepper_steps);
    spooling = 1; 
    //stepper.step(-stepper_steps);   // Spooling motor on slave!
    stepperstepcounter=stepperstepcounter+stepper_steps;
//    writeoutput(970,stepperstepcounter); // Use only when testing with 1 arduino!    
    break;
    
  case 430: //Move spool sideways
    spoolsidewaymovements++;
    
    if (stepper2_position+(stepper2_direction*stepper2_sidesteps)>stepper2_freearea){
      stepper2_direction=-1;
    }
    
    if (stepper2_position+(stepper2_direction*stepper2_sidesteps)<0){
      stepper2_direction=+1;
    }

    if (-1*stepper2_direction > 0) {                                        //Sergiy style spooling : this way 21 movements, freearea = 21000{
      stepper2_position=stepper2_position+stepper2_direction*stepper2_sidesteps*20;
      stepper2.step((-1*stepper2_direction*stepper2_sidesteps*20)-spoolsidewaycorrection); // -1 : start from right to left (for the flight model spool)
    }                                                                 // spoolsidewaycorrection : Correct for positioning each step
    else if (-1*stepper2_direction < 0) {               
      stepper2_position=stepper2_position+stepper2_direction*stepper2_sidesteps;
      stepper2.step(-1*stepper2_direction*stepper2_sidesteps); // -1 : start from right to left (for the flight model spool)
    }                                                                       // } Sergiy style spooling: this way 420 movements, freearea = 21000
    
//    Normal spooling:    
//    stepper2_position=stepper2_position+stepper2_direction*stepper2_sidesteps;
//    stepper2.step(-1*stepper2_direction*stepper2_sidesteps); // -1 : start from right to left (for the flight model spool)
    
    digitalWrite(pin_stepper2_1, LOW);
    digitalWrite(pin_stepper2_2, LOW);
    digitalWrite(pin_stepper2_3, LOW);    
    digitalWrite(pin_stepper2_4, LOW);    
    statusmsg('430');
    writeoutput(940,stepper2_position);
    break;
    
  case 440: //Spool and move sideways at the same time NOT WORKING!
    //stepper.step((-stepper_steps));   // Spooling motor on slave!
    stepper2.step((stepper2_sidesteps));
    digitalWrite(pin_stepper2_1, LOW);
    digitalWrite(pin_stepper2_2, LOW);
    digitalWrite(pin_stepper2_3, LOW);    
    digitalWrite(pin_stepper2_4, LOW);    

//    digitalWrite(pin_stepper1, LOW);  // Spooling motor on slave!
//    digitalWrite(pin_stepper2, LOW);  // Spooling motor on slave!
//    digitalWrite(pin_stepper3, LOW);  // Spooling motor on slave!
//    digitalWrite(pin_stepper4, LOW);  // Spooling motor on slave!
    statusmsg('440');
    break;    

  case 450: //Spool wire from one spool to another
    for (int i=0;i<57;i++){
    //stepper.step(515); // = 1 round   // Spooling motor on slave!
    stepper2.step(65); //50µm
    digitalWrite(pin_stepper2_1, LOW);
    digitalWrite(pin_stepper2_2, LOW);
    digitalWrite(pin_stepper2_3, LOW);    
    digitalWrite(pin_stepper2_4, LOW); 
    }
    statusmsg('450');
    break;    

  case 460: //Spool wire from one spool to another, the other direction
    for (int i=0;i<57;i++){
    //stepper.step(515);   // Spooling motor on slave!
    stepper2.step(-65); //50µm
    digitalWrite(pin_stepper2_1, LOW);
    digitalWrite(pin_stepper2_2, LOW);
    digitalWrite(pin_stepper2_3, LOW);    
    digitalWrite(pin_stepper2_4, LOW); 
    }
    statusmsg('460');
    break;    

  case 600: //int max_automatizationdelay=200; //ms between steps in automatic mode  
    max_automatizationdelay=command[1];
    break;
    
  case 610: //int stepdelay=300; //delay between process steps
    stepdelay=command[1];
    break;
    
  case 620://int stepper_speed=5; //rpm, speed of stepper
    stepper_speed=command[1];
    writeslaveoutput(620,stepper_speed);
//    stepper.setSpeed(stepper_speed); // Spooling motor on slave!
    break;
    
  case 630://int stepper_steps=40; // how many steps is the wire spooled between bonds
    stepper_steps=command[1];
    writeslaveoutput(630,stepper_steps); // Spooling motor on slave!
    break;
    
  case 640://int clamp1_open=130; servo position
    clamp1_open=command[1];
    break;
    
  case 650://int clamp1_close=60; servo position
    clamp1_close=command[1];
    break;
    
  case 660://int tension_off=80; servo position
    tension_off=command[1];
    break;
    
  case 670://int tension_on=20; servo position
    tension_on=command[1];
    break;
    
  case 680://int flattener_off=0; servo position
    flattener_off=command[1];
    break;

  case 685://int flattenerarm_in=; servo position
    flattenerarm_in=command[1];
    break;  

  case 690://int flattener_on=179; servo position
    flattener_on=command[1];
    break;

  case 695://int flattenerarmout=; servo position
    flattenerarm_out=command[1];
    break;
    
  case 700://int pin_off=133; servo position
    pin_off=command[1];
    break;
    
  case 710://int pin_on=123; servo position
    pin_on=command[1];
    break;
    
  case 720://int clamp2_open=179; servo position
    clamp2_open=command[1];
    break;
    
  case 730://int clamp2_close=50; servo position
    clamp2_close=command[1];
    break;
    
  case 740://int stepper2_sidesteps=2; how many steps the transl.stage is moved
    stepper2_sidesteps=command[1];
    break;
    
  case 741://int stepper2_freearea=100; How big area is allowed for spool sideway movement. Starting from one side.
    stepper2_freearea=command[1];
    break;
    
  case 750://int stepper2_speed=5; RPM how fast is the transl. stage moved
    stepper2_speed=command[1];
    stepper2.setSpeed(stepper2_speed);
    break;
    
  case 760: //query for number of bonds
    writeoutput(920,bonds);
    break;    
    
  case 770: //reset bond counter
    bonds=command[1];
    writeoutput(930,bonds);
    break;
    
  case 775: //reset stepperstepcounter  
    stepperstepcounter=command[1];
    writeoutput(970,stepperstepcounter);
    break;
    
  case 780: //TF sideway movement speed
    stepper3_speed=command[1];
//    stepper3.setSpeed(stepper3_speed);
    writeslaveoutput(780,stepper3_speed);
    break;
  
  case 790: //Distance between bonding positions for different wires (in steps)
    stepper3_distance=command[1];
    writeslaveoutput(790, stepper3_distance);
    break;
  
  case 792: // Release wire select stepper
    writeslaveoutput(792,0);
    break;
  
  case 795: //Select always the next wire to move to. For autorun mode.
    while (spooling==1) {        //
      readslaveinput(command);   //  While spooling on slave, wait.
      processstep(command[0]);   //  Comment this when testing with 1 arduino!
      delay(10);                //
    }                            //
  tfsidewaymovements++;

//  if (tfsidewaymovements % tfsidewaycorrectioncycles == 0) { //Correct positioning after 'tfsidewaycorrectioncycles' movements
//    tfsidewaycorrectionbool=true;
//  }
  
  switch (wirecounter){
    case 1:
        processstep(810);
        break;
    case 2:
        processstep(820);
        break;
    case 3:
        processstep(800);
        break;
    default: //
    ;
  }
  break;
  
  case 800: // Move TF to wire 1 (RIGHT)
    wselect = 1; 
//  switch (wirecounter){
//
//    case 1: // If we're already here, do nothing
//      break;
//    case 2: // If we're in the middle, move 1 distance 
//      stepper3.step(-stepper3_distance);
//      break;
//    case 3: // If we're in the left, move 2 distances
//      stepper3.step(-1*(stepper3_distance-2));
//      delay(10);
//      digitalWrite(pin_stepper3_1, LOW);
//      digitalWrite(pin_stepper3_2, LOW);
//      digitalWrite(pin_stepper3_3, LOW);    
//      digitalWrite(pin_stepper3_4, LOW);
//      delay(10);
//      stepper3.step(-1*(stepper3_distance-2));
//      break;
//    default: //
//      ;  
//  }
//    digitalWrite(pin_stepper3_1, LOW);
//    digitalWrite(pin_stepper3_2, LOW);
//    digitalWrite(pin_stepper3_3, LOW);    
//    digitalWrite(pin_stepper3_4, LOW);
    writeslaveoutput(800, command[1]);
//    wirecounter=1;
    break;
//
  case 810: // Move TF to wire 2 (CENTER)
    wselect = 1;
//  switch (wirecounter){
//
//    case 1: // If we're in the right, move 1 distances
//      stepper3.step(stepper3_distance);
//      break;
//    case 2: // If we're in the middle, do nothing
//      break;
//    case 3: // If we're in the right, move 1 distances
//      stepper3.step(-1*stepper3_distance);
//      break;
//    default: //
//      ;    
//  }
//    digitalWrite(pin_stepper3_1, LOW);
//    digitalWrite(pin_stepper3_2, LOW);
//    digitalWrite(pin_stepper3_3, LOW);    
//    digitalWrite(pin_stepper3_4, LOW);
    writeslaveoutput(810, command[1]);
//    wirecounter=2;
    break;
//    
  case 820: // Move TF to wire 3 (LEFT)
    wselect = 1; 
//  switch (wirecounter){
//
//    case 1: // If we're in the left, move 2 distances
//      stepper3.step(1*stepper3_distance);
//      delay(10);
//      digitalWrite(pin_stepper3_1, LOW);
//      digitalWrite(pin_stepper3_2, LOW);
//      digitalWrite(pin_stepper3_3, LOW);    
//      digitalWrite(pin_stepper3_4, LOW);
//      delay(10);
//      stepper3.step(1*stepper3_distance);
//      break;
//    case 2: // If we're in the middle, move 1 distance 
//      if (tfsidewaycorrectionbool==true) { // Correct tfsidewayposition every 35 cycles
//        stepper3.step(stepper3_distance+tfsidewaycorrection);
//        tfsidewaycorrectionbool=false;
//      }
//      else {
//        stepper3.step(stepper3_distance);
//      }
//      break;
//    case 3: // If we're in the right, do nothing
//      break;
//    default: //
//      ;    
//  }
//    digitalWrite(pin_stepper3_1, LOW);
//    digitalWrite(pin_stepper3_2, LOW);
//    digitalWrite(pin_stepper3_3, LOW);    
//    digitalWrite(pin_stepper3_4, LOW);
    writeslaveoutput(820, command[1]);
//    wirecounter=3;
    break;
    
//  case 830: // Calibration of TF: move left one step
//    writeslaveoutput(800,1);
////    stepper3.step(1);
//    break;
//    
//  case 840: // Calibration of TF: move right one step
//    writeslaveoutput(810,1);
////    stepper3.step(-1);
//    break;
    
  case 850: //reset spool movement progressbar 
    stepper2_position=0;
    stepper2_direction=1;
    writeoutput(940,stepper2_position);
    break;
    
  case 860: //Message from Slave - Done spooling!
    spooling = 0;  
    writeoutput(970,stepperstepcounter);
    break;
    
  case 865: //Message from Slave - Wire select done!
    wselect = 0;
    wirecounter = command[1];
    writeoutput(950,wirecounter); //send command 950 after a wire change
    break;
    
  case 870: //Autofix bad bond
    autofixsteps = command[1];
    autofix(autofixsteps);
    break;

  case 880: //Adjust tfsidewaycorrectioncycles
    tfsidewaycorrectioncycles = command[1];
    break;
    
  case 885: //Adjust spoolsidewaycorrection
    spoolsidewaycorrection = command[1];
    break;
    
    /*
    SIDEWAY MOVEMENT: 
    7.75mm = 10 000 steps
    -> 0.775µm / step
    100 steps = 77.5µm
    95 steps = 75.2µm
    65 steps = 50.4µm
    
    SPOOLING:
    Diameter = 53mm
    Circle = 166.5mm
    2 rounds = 1030 steps = 333mm
    0.32mm / step
    
    30.03mm = 94 steps
    19.84mm = 62 steps
    15.04mm = 47 steps
    10.24mm = 32 steps
    5.12mm = 16 steps
    */
    
  default: //TAKE NEXT STEP

    ;
  }
}











//****************************
// LOOP TO WAIT FOR COMMANDS
//****************************
void waitforstep(unsigned int command[]){

  //Read for new parameter
  do {
    while (spooling==1) {        //
      readslaveinput(command);   //  While spooling on slave, wait.
      processstep(command[0]);   //  Comment this when testing with 1 arduino!
      delay(10);                //
    }                            //
    while (wselect==1) {        //
      readslaveinput(command);   //  While wselect on slave, wait.
      processstep(command[0]);   //  Comment this when testing with 1 arduino!
      delay(10);                //
    }                            //    
    readinput(command);
    processstep(command[0]);

  }
  while((command[0]!=100)&&(automaticmode==false));

}


//*****************************
// CHECK IF INSPECTION SYSTEM ONLINE
//*****************************
void checkforinspectionsystem() {
  camerapinstate = analogRead(camerapin);
  if (camerapinstate < 750) {
    inspectiononline = 1;
    writeoutput(995, inspectiononline);
  }
  else {
    inspectiononline = 0;
    writeoutput(995, inspectiononline);
  }
}
  


//*****************************
// SEND INFO ABOUT CYCLE POSITION
//*****************************
void sendcycleposition(int pos) {
  writeoutput(910, pos);
}



//*******************************
//AUTOFIX BAD BONDS
//*******************************
void autofix(int steps) {
  delay(3000);
  //Check if wedge or lights down
  camerapinstate = analogRead(camerapin);
  if (camerapinstate < 750) {
    automaticmode=false;
    wedgeorlightdowncount++;
    writeoutput(985, wedgeorlightdowncount);
    return;
  }
  processstep(380); //Remove pin
  processstep(390); //Remove tension
  processstep(400); //Open Clamp 1
  processstep(410); //Open Clamp 2
  writeslaveoutput(420,steps); //Spooling
  spooling = 1; //Spooling 
  stepperstepcounter=stepperstepcounter+steps; //Spooling
  delay(stepdelay);
      while (spooling==1) {        //
        readslaveinput(command);   //  While spooling on slave, wait.
        processstep(command[0]);   //  
        delay(10);                //
      }
  delay(1500);
  processstep(200); //Close clamp 1
  processstep(210); //Close clamp 2
  processstep(220); //Lower tension arm
  processstep(230); //Show flattener
  processstep(240); //FLATTEN!
  processstep(250); //Raise flattener
  processstep(260); //Remove flattener
  processstep(270), //Insert pin
  delay(stepdelay);
              processstep(300); //Lower w. for upper flat.
              delay(700); 
              processstep(310); //Flatten upper wire
              delay(700);
              processstep(340); //Lower to 2nd b. search height
              delay(1100);
              digitalWrite(pin_bonderswitch,LOW); //BOND
  delay(900); //Delay to give camera enough time to react
  // Check for bad bonds!
  camerapinstate = analogRead(camerapin);
  Serial.println(camerapinstate);
  if (camerapinstate < 750) {
      badbondcount++;
      writeoutput(980, badbondcount); //report failed autofix!
      automaticmode=false;
    }
  else {
    writeoutput(982, 0); //report a good autofix!
  }

}

//****************************
// SEND STATUS MESSAGES OVER SERIAL
//****************************


void statusmsg(int msg){
  if (sendstatus==true){
    writeoutput(990, msg);
  }
}


//****************************
// SENDING SERIAL OUTPUT
//****************************

void writeoutput(unsigned int command,unsigned long data){
  String datastring=String(data);
  int datalength=datastring.length();
  String outputstring="!" + String(command) + String(datalength)+String(data);
  Serial.println(outputstring);
}


//****************************
// SENDING SERIAL OUTPUT TO SLAVE
//****************************

void writeslaveoutput(unsigned int command,unsigned int data){
  String datastring=String(data);
  int datalength=datastring.length();
  String outputstring="!" + String(command) + String(datalength)+String(data);
  Serial1.println(outputstring);
}










//****************************
// READING SERIAL INPUT - USB/PC
//****************************

void readinput(unsigned int data[]){
  if (Serial.peek()!=-1) {
    //INIT
    char cmd[10];
    memset(cmd, '\0', 10);
    char value[10];
    memset(value, '\0', 10);
    char  length_in[10];
    memset(length_in, '\0', 10);   
    unsigned int length;
    byte inByte = '\0';

    while(inByte != '!') {
      inByte = Serial.read(); // Wait for the start of the message
    }

    //MAKING SURE A DECENT INPUT
    if(inByte == '!') {

      //READING COMMAND PART
      while(Serial.available() < 3) { // Wait until we receive enough characters
        ;
      }

      for (int i=0; i < 3; i++) {
        cmd[i] = Serial.read(); // Read the characters into an array
      }

      data[0]=atoi(cmd);

      //READING LENGTH OF VALUE
      while(Serial.available() < 1) { // Wait until we receive enough characters
        ;
      }
      length_in[0]=Serial.read();
      length=atoi(length_in);

      //READING VALUE, MAX VALUE IS 65535!!!
      while(Serial.available() < length) { // Wait until we receive enough characters
        ;
      }

      for(int i=0; i<length; i++){
        value[i]=Serial.read();
      }

      data[1]=atoi(value);    
    }
  }
  //IF NO DATA INPUTTED, RESETTING data
  else{
    data[0]=0;
  }
}

//****************************
// READING SERIAL INPUT - SLAVE ARDUINO
//****************************

void readslaveinput(unsigned int data[]){
  if (Serial1.peek()!=-1) {
    //INIT
    char cmd[10];
    memset(cmd, '\0', 10);
    char value[10];
    memset(value, '\0', 10);
    char  length_in[10];
    memset(length_in, '\0', 10);   
    unsigned int length;
    byte inByte = '\0';

    while(inByte != '!') {
      inByte = Serial1.read(); // Wait for the start of the message
    }

    //MAKING SURE A DECENT INPUT
    if(inByte == '!') {

      //READING COMMAND PART
      while(Serial1.available() < 3) { // Wait until we receive enough characters
        ;
      }

      for (int i=0; i < 3; i++) {
        cmd[i] = Serial1.read(); // Read the characters into an array
      }

      data[0]=atoi(cmd);

      //READING LENGTH OF VALUE
      while(Serial1.available() < 1) { // Wait until we receive enough characters
        ;
      }
      length_in[0]=Serial1.read();
      length=atoi(length_in);

      //READING VALUE, MAX VALUE IS 65535!!!
      while(Serial1.available() < length) { // Wait until we receive enough characters
        ;
      }

      for(int i=0; i<length; i++){
        value[i]=Serial1.read();
      }

      data[1]=atoi(value);    
    }
  }
  //IF NO DATA INPUTTED, RESETTING data
  else{
    data[0]=0;
  }
}


///****************************************************
/// ATTACH / DETTACH THE SERVOS BASED ON USER INPUT
///****************************************************
void connectservos(boolean servoconnection){
  if (servoconnection == true){
    //Attach servos to pins
    servo_clamp1.attach(pin_servo1);
    servo_tension.attach(pin_servo2);
    servo_flattener.attach(pin_servo3);
    servo_clamp2.attach(pin_servo4);
    servo_pin.attach(pin_servo5);
    servo_flattenerarm.attach(pin_servo6);
    //Write initial values:
    //flattener up
    servo_flattener.write(flattener_off);
    delay(100); 
    // Remove flattener arm
    servo_flattenerarm.write(flattenerarm_out);
    delay(100); 
    //remove pin
    servo_pin.write(pin_off);
    delay(100); 
    //raise tension arm
    servo_tension.write(tension_off);
    delay(100); 
    //open clamp2
    servo_clamp2.write(clamp2_open);  
    delay(100); 
    //open clamp1
    servo_clamp1.write(clamp1_open);
    delay(100); 
  }
  else if (servoconnection == false) {
    servo_clamp1.detach();
    servo_tension.detach();
    servo_flattener.detach();
    servo_clamp2.detach();
    servo_pin.detach();
    servo_flattenerarm.detach();
  }
}



