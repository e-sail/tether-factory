/*
  Control camera / serial connection for use with TF4/5
 */

char inByte = '0';         // incoming serial byte
int led = 13;
int pin = 9;
const int check_pin = A8;
char previousstate = '0';

void setup()
{
  Serial.begin(115200);
  pinMode(led, OUTPUT);
  pinMode(pin, OUTPUT);
//  pinMode(check_pin, INPUT_PULLUP);    

///INITIAL POSITIONS
  digitalWrite(led, LOW); 
  digitalWrite(pin, HIGH); // Possible to use Arduino internal Pullup resistor on master
}

void loop()
{
  // if we get a valid byte, read analog ins:
  if (Serial.available() > 0) {
    inByte = Serial.read();
    delay(10);
  }
  if (inByte == '1') { // Turn Led & Digital pin "ON"
    if (previousstate == '0') {
      digitalWrite(led, HIGH);
      digitalWrite(pin, LOW);  // Possible to use Arduino internal Pullup resistor on master
//      Serial.println("1");
      previousstate = '1';
    }
  }
  else if (inByte == '0') { // Turn Led & Digital pin "OFF"
    if (previousstate == '1') {
      digitalWrite(led, LOW);
      digitalWrite(pin, HIGH); // Possible to use Arduino internal Pullup resistor on master
//      Serial.println("0");
      previousstate = '0';
    }
  }
//  delay(1000);
//  Serial.println(digitalRead(check_pin));


}

