/*
  E-Sail tether factory control software
 
 Copyright 2011 Risto H. Kurppa risto at kurppa . fi under the GPLv3+ License
           2012 Timo Rauhala trauhala at gmail . com under the GPLv3+ License
           
 GPL3 LICENSE
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
 
 */

// INCLUDES
#include <Servo.h> 
#include <Stepper.h>
#include <AFMotor.h>
//see https://github.com/adafruit/Adafruit-Motor-Shield-library for AFMotor library




// ************************
//  AUTOMATIZATION PARAMETERS, INITIAL VALUES
// ************************
boolean debugstatus=false; //Are debug messages sent over Serial or not

boolean automaticmode=false;

//TETHER FACTORY PARAMETERS

int max_automatizationdelay=2000; //ms between steps in automatic mode  
int stepdelay=300; //delay between steps

int stepper_speed=5; //rpm, speed of stepper
int stepper_steps=20; // how many steps is the wire spooled between bonds

int stepper2_speed=40; //how fast is stepper2
int stepper2_sidesteps=172; //how many steps stepper2


// ************************
//  PINS & OTHER CONSTANT VARIABLES 
// ************************

int bonding_on=50;
int bonding_off=90;


//STEPPER step counts per one round
const int STEPS = 192;
const int STEPS2 = 200;

//Precompiler fills in __DATE__ and  __TIME__
const char compiledate[] = __DATE__; 
const char compiletime[] = __TIME__; 

const int pin_led=13;

// ************************
//  RUNTIME VARIABLES 
// ************************

//STATUS VARIABLES
unsigned int command[2];
unsigned int rank = 2; // 1 == Master, 2 == Slave

unsigned int wirecounter=2; //This knows/records on what upper wire position we are, 1, 2 or 3. 0=uninitialized




// ***************************
// Initializing classes
// ***************************

AF_Stepper stepper(STEPS, 2);
AF_Stepper stepper2(STEPS2, 1);


//****************************
// Setup servos, motors etcetc
//****************************
void setup() {

  //SERIAL
  Serial1.begin(115200);

  //STEPPER
  stepper.setSpeed(stepper_speed);
  stepper2.setSpeed(stepper2_speed);
  

  //LED PIN
  pinMode(pin_led, OUTPUT); 
  
}


//****************************
// MAIN PROGRAM LOOP
//****************************

void loop() {
  waitforstep(command);
}


//****************************
// ALL STEPS THE TF CAN DO! pls see reference.txt
//****************************
void processstep(int step){
  delay(stepdelay);
  switch (step){

  case 110: //TEST, returns string over serial
    Serial.println("Magic");
    break;
    
  case 111: //returns master(0)/slave(1) string over serial
    writeoutput(960,rank); // Use command 960 for telling UI where we are connected.
    break;
 
  case 112: //returns current wirenumber
    writeoutput(113,wirecounter);
    break;
        
  case 120: //TEST, retursn !92048888 over serial (=8888 completed bonds!)
    writeoutput(920,8888);
    break;
    
    
  case 420: //Spool
    stepper.step(command[1],FORWARD,MICROSTEP); // Use AF_Stepper!
//    stepper.release(); // AF_Stepper! Do NOT release the spooling motor. 
    writeoutput(860,1); // Tell Master that we are done spooling
    break;
    

   case 620://int stepper_speed=5; //rpm, speed of stepper
    stepper_speed=command[1];
    stepper.setSpeed(stepper_speed);
    break;
    
  case 630://int stepper_steps=40; // how many steps is the wire spooled between bonds
    stepper_steps=command[1];
    break;
     
  case 790://int stepper2_sidesteps=2; how many steps stepper2
    stepper2_sidesteps=command[1];
    break;
    
  case 792: // Release wire select stepper
    stepper2.release();
    break;

  case 780://int stepper2_speed=5; RPM how fast is stepper2
    stepper2_speed=command[1];
    stepper2.setSpeed(stepper2_speed);
    break;


  case 800: // Move TF to wire 1 (RIGHT)
  switch (wirecounter){

    case 1: // If we're already here, do nothing
      break;
    case 2: // If we're in the middle, move 1 distance 
      stepper2.step(stepper2_sidesteps,FORWARD,SINGLE);
      break;
    case 3: // If we're in the left, move 2 distances
      stepper2.step(stepper2_sidesteps,FORWARD,SINGLE);
      stepper2.step(stepper2_sidesteps,FORWARD,SINGLE);
      break;
    default: //
      ;  
  }
    wirecounter=1;
    writeoutput(865,wirecounter); //send message to master, wselect done.
    break;

  case 810: // Move TF to wire 2 (CENTER)
  switch (wirecounter){

    case 1: // If we're in the right, move 1 distances
      stepper2.step(stepper2_sidesteps,BACKWARD,SINGLE);
      break;
    case 2: // If we're in the middle, do nothing
      break;
    case 3: // If we're in the right, move 1 distances
      stepper2.step(stepper2_sidesteps,FORWARD,SINGLE);
      break;
    default: //
      ;    
  }
    wirecounter=2;
    writeoutput(865,wirecounter); //send message to master, wselect done.
    break;
    
  case 820: // Move TF to wire 3 (LEFT)
  switch (wirecounter){

    case 1: // If we're in the left, move 2 distances
      stepper2.step(stepper2_sidesteps,BACKWARD,SINGLE);
      stepper2.step(stepper2_sidesteps,BACKWARD,SINGLE);
      break;
    case 2: // If we're in the middle, move 1 distance 
      stepper2.step(stepper2_sidesteps,BACKWARD,SINGLE);
      break;
    case 3: // If we're in the right, do nothing
      break;
    default: //
      ;    
  }
    wirecounter=3;
    writeoutput(865,wirecounter); //send message to master, wselect done.
    break;

    
    
    /*
    SIDEWAY MOVEMENT: 
     7.75mm = 10 000 steps
     -> 0.775µm / step
     100 steps = 77.5µm
     95 steps = 75.2µm
     65 steps = 50.4µm
     
     SPOOLING:
     Diameter = 53mm
     Circle = 166.5mm
     2 rounds = 1030 steps = 333mm
     1 round = 515 stepse = 166mm
     0.32mm / step
     
     30.03mm = 94 steps
     19.84mm = 62 steps
     15.04mm = 47 steps
     10.24mm = 32 steps
     5.12mm = 16 steps
     */

  default: //TAKE NEXT STEP
  ;
  }
  
}











//****************************
// LOOP TO WAIT FOR COMMANDS
//****************************
void waitforstep(unsigned int command[]){

  //Read for new parameter
  do {
    readinput(command);

  
    processstep(command[0]);

  }
  while((command[0]!=100)&&(automaticmode==false));

}






//****************************
// SEND DEBUG MESSAGES OVER SERIAL
//****************************


void debugmsg(String message){
  if (debugstatus==true){
    Serial1.println(message);
  }
}











//****************************
// SENDING SERIAL OUTPUT
//****************************

void writeoutput(unsigned int command,unsigned int data){
  String datastring=String(data);
  int datalength=datastring.length();
  String outputstring="!" + String(command) + String(datalength)+String(data);
  Serial1.println(outputstring);
}






//****************************
// READING SERIAL INPUT FOR COMMANDS 
// format: !XXXYZZZZ where XX = three digit command (100-999), Y = length of value (1-9), ZZZZ=value (0-about 63000 or so..)
//****************************

void readinput(unsigned int data[]){
  if (Serial1.peek()!=-1) {
    //INIT
    char cmd[10];
    memset(cmd, '\0', 10);
    char value[10];
    memset(value, '\0', 10);
    char  length_in[10];
    memset(length_in, '\0', 10);   
    unsigned int length;
    byte inByte = '\0';

    while(inByte != '!') {
      inByte = Serial1.read(); // Wait for the start of the message
    }

    //MAKING SURE A DECENT INPUT
    if(inByte == '!') {

      //READING COMMAND PART
      while(Serial1.available() < 3) { // Wait until we receive enough characters
        ;
      }

      for (int i=0; i < 3; i++) {
        cmd[i] = Serial1.read(); // Read the characters into an array
      }

      data[0]=atoi(cmd);

      //READING LENGTH OF VALUE
      while(Serial1.available() < 1) { // Wait until we receive enough characters
        ;
      }
      length_in[0]=Serial1.read();
      length=atoi(length_in);

      //READING VALUE, MAX VALUE IS 65535!!!
      while(Serial1.available() < length) { // Wait until we receive enough characters
        ;
      }

      for(int i=0; i<length; i++){
        value[i]=Serial1.read();
      }

      data[1]=atoi(value);    
    }
  }
  //IF NO DATA INPUTTED, RESETTING data
  else{
    data[0]=0;
  }
}

