/*
  ELECTRIC SOLAR SAIL TETHER FACTORY CONTROL SOFTWARE

  TODO:
   //Sarjaportti  
  //Serial.begin(9600); - DISABLOI DIGIPINNIT 0 ja 1!!
  / SHIELDISTÄ TARVII SIIRTÄÄ KAHDEN SERVON PINNIT ANALOGIPINNEIKS
  // KTS http://www.arduino.cc/en/Tutorial/AnalogInputPins (analogipinnit -> digipinneiks)
 
2010-12-10
  * added steppersteps to count steps
  * LCD now shows clamp, stepper+/- flattener XX, steps, C1, C2, Tens


*/
 
// INCLUDES
#include <LiquidCrystal.h>
#include <Servo.h> 
#include <Stepper.h>
#define STEPS 192



  //Precompiler fills in __DATE__ and  __TIME__
  char compiledate[] = __DATE__; 
  char compiletime[] = __TIME__; 
  
//   INPUT  --------------------

//ANALOG INPUT PINS!!
int pin_pot1=7; //Clamp 1
int pin_pot2=6; //Tension
int pin_pot3=5; //Flattener
int pin_pot4=4; //Clamp 2
int pin_pot5=3; // Pin

//DIGITAL INPUT
int pin_button1=18; //Stepper +
int pin_button2=17; //Stepper -
int pin_button3=16; //Flattener tilt solenoid
int pin_button4=19; //Bonder clamp


//   DIGI OUTPUT --------------------


//LCD CONTROL PINS
int pin_reset=23;
int pin_enable=25;
int pin_rw=27;
int pin_d4=31;
int pin_d5=29;
int pin_d6=35;
int pin_d7=33;

//DIGITAL OUTPUT //8-13 vapaat (mut ei kontakteja..
int pin_servo1=A1; //SIIRRETTY ANALOGIAPORTTIIN -> Serial0 vapaana
int pin_servo2=A0; //SIIRRETTY ANALOGIAPORTTIIN -> Serial0 vapaana
int pin_servo3=2;
int pin_servo4=14;
int pin_servo5=15;

int pin_stepper1=3;
int pin_stepper2=5;
int pin_stepper3=4;
int pin_stepper4=6;

//int pin_solenoid=7; 
int pin_clamp=7; //TARVII SIIRTÄÄ JOHONKIN!! Tarvii PWM:n ULN:n yli..

int pin_led=13;

//INIT
LiquidCrystal lcd(pin_reset, pin_enable, pin_d4, pin_d5, pin_d6, pin_d7);
Servo servo1;  // create servo object to control a servo 
Servo servo2;  // create servo object to control a servo 
Servo servo3;  // create servo object to control a servo 
Servo servo4;  // create servo object to control a servo 
Servo servo5;  // create servo object to control a servo 
Stepper stepper(STEPS, pin_stepper1, pin_stepper2, pin_stepper3, pin_stepper4);

//STATUSMUUTTUJAT

int solenoid_on=0;
int clamp_on=0;
unsigned long steppersteps=0;

//POTIKOIDEN LUKUMUUTTUJAT
int pot1_val;
int pot2_val;
int pot3_val;
int pot4_val;
int pot5_val;

int pot1_val_skaalattu;
int pot2_val_skaalattu;
int pot3_val_skaalattu;
int pot4_val_skaalattu;
int pot5_val_skaalattu;


















void setup() {
  
Serial.begin(9600);
  
  //SERVOT
  servo1.attach(pin_servo1);
  servo2.attach(pin_servo2);
  servo3.attach(pin_servo3);
  servo4.attach(pin_servo4);
  servo5.attach(pin_servo5);
  
  //BUTTONS
  pinMode(pin_button1,INPUT);
  pinMode(pin_button2,INPUT);
  pinMode(pin_button3,INPUT);  
  pinMode(pin_button4,INPUT);  

  //Sisäiset ylösvetovastukset
  digitalWrite(pin_button1,HIGH);
  digitalWrite(pin_button2,HIGH);
  digitalWrite(pin_button3,HIGH);  
  digitalWrite(pin_button4,HIGH);    

  
  //LCD ALUSTUS JA KIRJOITUS
  pinMode(pin_rw, OUTPUT);
  digitalWrite(pin_rw, LOW);
  // set up the LCD's number of rows and columns: 
  lcd.begin(40, 2);

  //STEPPER
  stepper.setSpeed(5);
  //stepper.setSpeed(200); //LANGAN KELAUKSEEN
  
  //LED
  pinMode(pin_led, OUTPUT);    

  //SOLENOID
  //pinMode(pin_solenoid, OUTPUT);
  //digitalWrite(pin_solenoid,LOW);

  //pinMode(pin_clamp, OUTPUT);
  //analogWrite(pin_clamp, 170);
  //digitalWrite(pin_clamp,LOW);
  
    //LCD 
  lcd.clear();
  lcd.setCursor(0, 0);

   //Printing date one char at the time
  for(int i=0;i<strlen(compiledate);++i){
    lcd.print(compiledate[i]);
  }
  
    lcd.print("  ");

  //Printing time one char at the time  
  for(int i=0;i<strlen(compiletime);++i){
    lcd.print(compiletime[i]);
  }
 
  delay(3000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Clamp:closed");

  lcd.setCursor(14, 0);
  lcd.print("Stepper: ");   

  lcd.setCursor(27, 0);
  lcd.print("Flattener:XX");
}

















void loop() {
  
  //READ BUTTON STATES
  int button1_state = digitalRead(pin_button1);
  int button2_state = digitalRead(pin_button2);  
  int button3_state = digitalRead(pin_button3);  
  int button4_state = digitalRead(pin_button4);    
  
  //READ POTENTIOMETER VALUES
  pot1_val = analogRead(pin_pot1);
  pot2_val = analogRead(pin_pot2);  
  pot3_val = analogRead(pin_pot3);  
  pot4_val = analogRead(pin_pot4);  
  pot5_val = analogRead(pin_pot5);  
  
  //SCALE POTENTIOMETER VALUES
  pot1_val_skaalattu = map(pot1_val, 0, 1023, 1, 30); // C1
  pot2_val_skaalattu = map(pot2_val, 0, 1023, 0, 179); //tens
  pot3_val_skaalattu = map(pot3_val, 0, 1023, 0, 179); //flat
  pot4_val_skaalattu = map(pot4_val, 0, 1023, 0, 179); //c2
  pot5_val_skaalattu = map(pot5_val, 0, 1023, 0, 179); //pin

  
  
//ALARIVILLÄ KÄVELLYT STEPIT JA CLAMPIEN JA KIRISTÄJÄN STATUS

  lcd.setCursor(0, 0);

 // lcd.print(steppersteps);
 // lcd.print(" steps");
//
//
//  lcd.setCursor(16, 1);
//  lcd.print("C1:");  
//  lcd.print(pot1_val_skaalattu);
//  servo1.write(pot1_val_skaalattu);
//
//  if (pot1_val_skaalattu<100){
//  lcd.print(" ");}
//  if (pot1_val_skaalattu<10){
//  lcd.print(" ");}
//
//
//  lcd.setCursor(25, 1);
//  lcd.print("C2:");
//  lcd.print(pot4_val_skaalattu);
//  servo4.write(pot4_val_skaalattu);
//
//  if (pot4_val_skaalattu<100){
//  lcd.print(" ");}
//  if (pot4_val_skaalattu<10){
//  lcd.print(" ");}
//
//
//  lcd.setCursor(32, 1);
//  lcd.print("Tens:");
//  lcd.print(pot2_val_skaalattu);
//  servo2.write(pot2_val_skaalattu);
//
//  if (pot2_val_skaalattu<100){
//  lcd.print(" ");}
//  if (pot2_val_skaalattu<10){
//  lcd.print(" ");}


// KAIKKIEN LUKEMIEN NÄYTTÖ DISABLOITU  
  //NÄYTETÄÄN SERVOJEN LUKEMAT
  lcd.setCursor(0, 1);

  lcd.print("C1:");  
  lcd.print(pot1_val_skaalattu);
  //servo1.write(pot1_val_skaalattu);
  
    //STEPPER
  stepper.setSpeed(pot1_val_skaalattu);

  if (pot1_val_skaalattu<100){
  lcd.print(" ");}
  if (pot1_val_skaalattu<10){
  lcd.print(" ");}
  
  lcd.setCursor(7, 1);
  lcd.print("Tens:");
  lcd.print(pot2_val_skaalattu);
  servo2.write(pot2_val_skaalattu);

  if (pot2_val_skaalattu<100){
  lcd.print(" ");}
  if (pot2_val_skaalattu<10){
  lcd.print(" ");}
  
  lcd.setCursor(16, 1);
  lcd.print("Flat:");
  lcd.print(pot3_val_skaalattu);
  servo3.write(pot3_val_skaalattu);

  if (pot3_val_skaalattu<100){
  lcd.print(" ");}
  if (pot3_val_skaalattu<10){
  lcd.print(" ");}
  
  lcd.setCursor(25, 1);
  lcd.print("C2:");
  lcd.print(pot4_val_skaalattu);
  servo4.write(pot4_val_skaalattu);

  if (pot4_val_skaalattu<100){
  lcd.print(" ");}
  if (pot4_val_skaalattu<10){
  lcd.print(" ");}
  
  lcd.setCursor(32, 1);  
  lcd.print("Pin:");
  lcd.print(pot5_val_skaalattu);
  servo5.write(pot5_val_skaalattu);

  if (pot5_val_skaalattu<100){
  lcd.print(" ");}
  if (pot5_val_skaalattu<10){
  lcd.print(" ");}
  
  // STEPPER CONTROL +/-
  if (button1_state == LOW) {
    lcd.setCursor(14, 0);
    lcd.print("Stepper:+");
    stepper.step(1);
    steppersteps=steppersteps+1;
    //delay(300); DELAYSTA HAITTAA
    lcd.setCursor(14, 0);
    lcd.print("Stepper: "); 
  } 
  
  if (button2_state == LOW) {
    lcd.setCursor(14, 0);
    lcd.print("Stepper:-");
    stepper.step(-1);
    steppersteps=steppersteps-1;
    //delay(300); DELAYSTA HAITTAA
    lcd.setCursor(14, 0);
    lcd.print("Stepper: "); 
  } 

  //SOLENOID TO TILT FLATTENER
//  if (button3_state == LOW){
//    if (solenoid_on==0) {     
//      lcd.setCursor(27, 0);
//      lcd.print("Flattener:xx");
//      //lcd.print("Flattener:in ");
//      //digitalWrite(pin_solenoid,HIGH);
//      solenoid_on=1;
//      delay(300);
//    }
//    else if (solenoid_on==1) {
//      lcd.setCursor(27, 0);
//      lcd.print("Flattener:XX");
//      //lcd.print("Flattener:out");
//      //digitalWrite(pin_solenoid,LOW);
//      solenoid_on=0;
//      delay(300);
//    }
//  }
    
    //BONDER CLAMP CONTROL ON/OFF
      if (button4_state == LOW){
//          if (clamp_on==0) {     
//            lcd.setCursor(0, 0);
//            lcd.print("Clamp:open  ");
//            //lcd.print("Flattener:in ");
//            //digitalWrite(pin_solenoid,HIGH);
//            analogWrite(pin_clamp, 170);
//            clamp_on=1;
//            delay(300);
//          
            Serial.println(" ");
            Serial.print("C1:");
            Serial.println(pot1_val_skaalattu);
            Serial.print("tens:");
            Serial.println(pot2_val_skaalattu);
            Serial.print("flat:");
            Serial.println(pot3_val_skaalattu);
            Serial.print("C2:");
            Serial.println(pot4_val_skaalattu);
            Serial.print("pin:");
            Serial.println(pot5_val_skaalattu);  
            delay(1000);
          }
//          else if (clamp_on==1) {
//            lcd.setCursor(0, 0);
//            lcd.print("Clamp:closed");
//            //lcd.print("Flattener:out");
//            //digitalWrite(pin_solenoid,LOW);
//            analogWrite(pin_clamp, 0);      
//            clamp_on=0;
//            delay(300);
//          }
//      }
  
}


