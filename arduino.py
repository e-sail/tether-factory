#!/usr/bin/env python

"""
Copyright 2011 Risto H. Kurppa risto @ kurppa . fi under the GPLv3+ License

Based on Arduino.py copyright 2009-2010 Akash Manohar J akash@akash.im under the MIT License
https://github.com/SingAlong/Python-Arduino-Prototyping-API/blob/master/arduino/arduino.py


GPL3 LICENSE
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.



MIT LICENSE
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


"""

import sys
import serial
import time

class Arduino(object):

    __OUTPUT_PINS = -1

    def __init__(self, port, baudrate=115200):
        self.serial = serial.Serial(port, baudrate)

    def __str__(self):
        return "Arduino is on port %s at %d baudrate" %(self.serial.port, self.serial.baudrate)

    def output(self, pinArray):
        self.__sendData(len(pinArray))

        if(isinstance(pinArray, list) or isinstance(pinArray, tuple)):
            self.__OUTPUT_PINS = pinArray
            for each_pin in pinArray:
                self.__sendPin(each_pin)
        return True

    def sendSer(self, serial_data):
        self.serial.write(str(serial_data))        
        #sys.stdout.write("Arduino: sent" + str(serial_data))
        return 0

    def getSer(self):
        input=''
        if(self.serial.inWaiting()!=0):
#            return self.serial.read(self.serial.inWaiting())
            return self.serial.readline()        
        #while (self.serial.inWaiting()!=0):
        #input=input+self.serial.read(1)
        #if(self.serial.inWaiting()!=0):
          #  return self.serial.read(1)
            #return self.serial.readline()#.replace("\r\n","")
        else:
            return "Nothing received"    
    
    def pollinput(self):
        return self.serial.inWaiting()
        
#    def setLow(self, pin):
#        self.__sendData('0')
#        self.__sendPin(pin)
#        return True
#
#    def setHigh(self, pin):
#        self.__sendData('1')
#        self.__sendPin(pin)
#        return True
#
#    def getState(self, pin):
#        self.__sendData('2')
#        self.__sendPin(pin)
#        return self.__formatPinState(self.__getData())
#
#    def analogWrite(self, pin, value):
#        self.__sendData('3')
#        hex_value = hex(value)[2:]
#        if(len(hex_value)==1):
#            self.__sendData('0')
#        else:
#            self.__sendData(hex_value[0])
#        self.__sendData(hex_value[1])
#        return True
#
#    def analogRead(self, pin):
#        self.__sendData('4')
#        self.__sendPin(pin)
#        return self.__getData()
#
#    def turnOff(self):
#        for each_pin in self.__OUTPUT_PINS:
#            self.setLow(each_pin)
#        return True
#
#    def __sendPin(self, pin):
#        pin_in_char = chr(pin+48)
#        self.__sendData(pin_in_char)

    def __sendData(self, serial_data):
        #while(self.__getData()!="what"):
        #    pass
        self.serial.write(str(serial_data))
        
    def __getSer(self):
        return self.serial.readline().replace("\r\n","")        

#
#    def __formatPinState(self, pinValue):
#        if pinValue=='1':
#            return True
#        else:
#            return False

    def disconnect(self):
        self.serial.close()
        return True

    
    def __del__(self):
        #close serial connection once program ends
        #this fixes the problem of port getting locked or unrecoverable in some linux systems
        self.serial.close()
    
