#!/usr/bin/python
# coding: utf-8

"""
Copyright 2011 Risto H. Kurppa risto at kurppa . fi under the GPLv3+ License

GPL3 LICENSE
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from arduino import Arduino
import sys
import serial
import struct
import logging


connected = 0


class TetherFactory:
    
    def isConnected(self):
		if connected==1:
			return 1
		else: 
			return 0
    
    def __init__(self, msg):
        self.logLevel = logging.WARNING
        global logger
        self.logger = logging.getLogger('tf')
        self.handler = logging.FileHandler('./tf.log')
        self.formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        self.handler.setFormatter(self.formatter)
        self.logger.addHandler(self.handler) 
        self.logger.setLevel(self.logLevel)
        self.msg = msg
        
        
    def changeLogLevel(self,level):
        if level == 0:
            self.logLevel = logging.INFO
            self.logger.setLevel(self.logLevel)
#            print "info"            
        elif level == 1:
            self.logLevel = logging.WARNING
            self.logger.setLevel(self.logLevel)
#            print "warning"
            
    def initialize(self,device): #Initialize arduino
        self.msg.write("Connecting to Arduino..")
        self.arduinoconn = Arduino(device)
        global connected
        connected = 1
#        self.msg.write("OK")
        self.logger.warning('Tf connected!')
        

    def close(self): #Initialize arduino
        self.msg.write("Disconnecting Arduino..")
        self.arduinoconn.disconnect()
        global connected
        connected = 0
        self.msg.write("OK")
        self.logger.warning('Tf disconnected!')
        
        
    def send(self,cmd,value): #generate struct
        value_maxlength=5
        #self.msg.write("SENDING..")
        message='!'+str(cmd)+str(len(str(value)))+str(value)
        self.arduinoconn.sendSer(message)
        if ((cmd != 111) and (cmd != 112)): #Don't write the initial values to textbrowser
            self.msg.write("Sent code " + message)
        #self.arduinoconn.sendSer(second)
           
    def receive(self): #read struct
        read=self.arduinoconn.getSer()
        self.msg.write(read)

    def receivecommand(self): #read struct
        #COMMAND FORMAT
        #!xxxzyyy where 
        #! = command starts
        #xxx = 100-999 command ID
        #z = 1-5, length of yyy
        #yyy = command value, using long as type now for longer commands
        read=self.arduinoconn.getSer()
        position=0
        offset=0
        commands=read.count('!')
        while commands>0: #if 1 or more commands found
            try:    
                for i in range(1,commands+1): #go through all commands
                    #find beginning of a command
                    position=read[offset:].find('!')
                    #extract command
                    commandnum=int(read[offset+position+1:offset+position+4])
                    #extract length of value
                    valuelength=int(read[offset+position+4])
                    #extract value
                    value=long(read[offset+position+5:offset+position+5+valuelength])
                    #self.msg.write("\n" + str(valuelength) +" "+ str(commandnum) + ":"+ str(value))
                    offset+=position+5+valuelength
                    #self.msg.write("\noffset: " + str(offset))
                    #self.msg.write("Received command: !" + str(commandnum) + str(valuelength) + str(value))
                    return commandnum,value
            except (IndexError, ValueError, StandardError):
                print "Received a corrupted command. " + str(read)
                try:
                    datanro = int(str(read).lstrip('!'))
                    return commands,datanro
                except (ValueError):
                    return commands,0
        else:
            return 0,0
        
        

    def pollinput(self):
        #self.msg.write("POLLING..")
        #RECEIVING FROM ARDUINO HERE
        #(first, second)=self.s.unpack(packed)
        #sys.stdout.write("\nReceived code " + first + ", value " + str(second) + "\n")
        chars=self.arduinoconn.pollinput()
        self.msg.write(str(chars)+ " bytes waiting in serial buffer")
        
	


def printmenu():
    sys.stdout.write("\n\nTether factory MENU \n")
    sys.stdout.write("1 = Initialize Arduino \n")
    sys.stdout.write("2 = Send custom command\n")
    sys.stdout.write("3 = Proceed to next step\n")    
    sys.stdout.write("4 = Read serial input\n")
    sys.stdout.write("5 = How many chars incoming \n")    
    sys.stdout.write("6 = Disconnect Arduino \n")    
    sys.stdout.write("7 = Receive command \n")
    sys.stdout.write("8 = Are we connected?\n")    
    sys.stdout.write("q =  quit\n")
    
def climenu():
  while 1==1:

    sys.stdout.write("\n")
    input=sys.stdin.readline()

    if input == "1\n":
        tf.initialize(defaultserialdevice)
        climenu()
    elif input =="2\n":
        cmd=raw_input("Give command (int 100..999) ")
        value=raw_input('Give value (int 0..65535)') 
        tf.send(cmd,value)
        climenu()
    elif input =="3\n":
        tf.send(100,0)
        climenu()
    elif input =="4\n":
        tf.receive()
        climenu()
    elif input =="5\n":
        tf.pollinput()
        climenu()      
    elif input =="6\n":
        tf.close()
        climenu()  
    elif input =="7\n":
        tf.receivecommand()
        climenu()
    elif input =="8\n":
        print str(tf.isConnected())
        climenu()      
    elif input =="q\n":
        sys.stdout.write("Quit\n")
        sys.exit(0)
    else:
        printmenu()


        
if __name__ == '__main__':
    defaultserialdevice='/dev/ttyUSB1'
    tf=TetherFactory(sys.stdout)
    tf.initialize(defaultserialdevice)
    printmenu()
    climenu()
