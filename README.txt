![](http://farm6.static.flickr.com/5129/5377053309_8c4ed3f2c6_m.jpg)

More images: http://www.flickr.com/photos/rhkrhk/5377053309/

Tether factory control software
==============================

Copyright 2011 Risto H. Kurppa risto at kurppa . fi under the GPLv3+ License

Description
-----------

I'm building a bridge to control a factory from PC:
pyQT - python - pyserial - serial port - Arduino - custom electronics - electromechanical parts (servos, steppers, LCD?)

The PC software is used to finetune ~40 process parameters and send them to the program running in Arduino. The main process is controlled by Arduino and the PC software only feeds commands to start/pause and parameters to adjust the behaviour. 


What does it do?
-----

1.  External power is connected to Arduino
2.  USB cable is connected to Arduino
3.  Arduino software starts and listens for serial connection and commands
4.  PC software is started. It asks for a configuration file
5.  Initial values are loaded in PC software
6.  User connects the PC software to Arduino over serial port
7.  User sends the process values to Arduino
8.  User commands Arduino to do an action
9.  Arduino receives the command, does the action and waits for next command

When I started this project in 01/2011 I had absolutely no python or pyQt experience. But as you see, it's really not that bad!


Files
-----
.gitignore - To ignore useless files (like python binaries)
arduino.py - Python code to talk to Arduino
Arduinocode/ - Arduino codes related to project
config.txt - sample configuration for tetherfactory-qt
create_reference.sh - script for Linux to grep accepted commands
manual.txt - short manual on how to use the setup / software
reference.txt - list of commands understood by current Arduino code. Don't edit this, it's upgraded by create_reference.sh
tetherfactory-qt.py - Code to run the graphical user interface
tetherfactory.py - Code to use the tetherfactory from command line
tfqtgui.py - PyQt code: dummy GUI for tether factory
tfqtgui.ui - Qt Designer file to design the GUI
TODO - list of items that need to be written
Videos/ - videos of tether factory working


What do you need to run/develop this?
-----

Ubuntu: sudo apt-get install git-core arduino python python-serial python-qt4 python-qt4-dev qt4-designer
Windows: TortoiseGit, Arduino and PythonXY

GIT

*  Version control system. This is what you need to download the source code from this repository
*  After installing you get the code by running: git clone git://gitorious.org/e-sail/tether-factory.git tether-factory (or do the same magic with graphical user interface)
*  GIT is a great software that allows one to save changes in the code and then push them to public repositories. Essential tool for code collaboration.
*  Linux: install git-core
*  Windows: see http://code.google.com/p/tortoisegit/
*  http://book.git-scm.com/
*  QGit is a nice tool for Linux to track changes in GIT tree: http://digilander.libero.it/mcostalba/

ARDUINO

*  Software to compile and flash the program to the microcontroller
*  We have an (old) Arduino Mega as the controller for the tether factory, with a custom shield that is the interface between Arduino and motors, servos and the rest.
*  Version 22 is needed.
*  Windows: http://arduino.cc/en/Main/Software
*  Linux: arduino arduino-core
*  http://www.arduino.cc/playground/

PYTHON

*  Python is the programming language the PC software is written in.
*  Linux: python python-serial
*  Windows: http://www.python.org/download/releases/ or see http://www.pythonxy.com/ to install almost everything you need
*  [Python Wikibook](http://en.wikibooks.org/wiki/Python_Programming)

PyQT

*  This is what I used to write the user interface. It's easy, don't worry. 
*  Linux: python-qt4 python-qt4-dev qt4-designer
*  Windows: http://www.riverbankcomputing.com/software/pyqt/download or see http://www.pythonxy.com/ to install almost everything you need
*  [PyQT tutorial](http://zetcode.com/tutorials/pyqt4/)
*  Qt4-designer was used to edit the UI

I used Geany to write the python code and Arduino for Arduino code.


If you have any questions, feel free to contact me: rhk at iki /dot/  fi


Links
-----

* [Markdown syntax](http://daringfireball.net/projects/markdown/basics)
* [Python, Arduino & Servos](http://principialabs.com/arduino-python-4-axis-servo-control/)

### Arduino
* [Arduino & IRC](http://blog.datasingularity.com/?p=148)
* [Arduino Firmata](http://www.arduino.cc/playground/Interfacing/Firmata)
* [Arduino Messenger](http://www.arduino.cc/playground/Code/Messenger)

