echo "E-SAIL TETHER FACTORY REFERENCE MANUAL" > reference.txt
echo "Don't edit this file, it is generated automatically!!" >> reference.txt
echo "The reference can be updated by running create_reference.sh" >> reference.txt
echo " " >> reference.txt
echo "These are the codes the Arduino code of tether factory understands." >> reference.txt
echo "Some of them require an additional parameter, for example !7003120 would store 120 as value of pin_off position." >> reference.txt
echo " " >> reference.txt

echo "Codes understood by the master shield:" >> reference.txt
cat Arduinocode/shield_tetherfactory_2_0/shield_tetherfactory_2_0.pde | grep case | sort >> reference.txt
sed -i 's/case/ /g' reference.txt
sed -i 's/int/ /g' reference.txt
sed -i 's/\// /g' reference.txt

sed -i -r 's/ +/ /g' reference.txt #if this doesn't work, use the following lines.. -r = extended regexp, not POSIX
#sed -i 's/  / /g' reference.txt
#sed -i 's/  / /g' reference.txt
#sed -i 's/  / /g' reference.txt
#sed -i 's/  / /g' reference.txt
#sed -i 's/  / /g' reference.txt

echo " " >> reference.txt
echo "Codes understood by the slave motor controller shield:" >> reference.txt
cat Arduinocode/shield_tetherfactory_slavemotor/shield_tetherfactory_slavemotor.pde | grep case | sort >> reference.txt
sed -i 's/case/ /g' reference.txt
sed -i 's/int/ /g' reference.txt
sed -i 's/\// /g' reference.txt

sed -i -r 's/ +/ /g' reference.txt #if this doesn't work, use the following lines.. -r = extended regexp, not POSIX


echo " " >> reference.txt
echo "Don't edit this file, it is generated automatically!!" >> reference.txt
echo "The reference can be updated by running create_reference.sh" >> reference.txt
cat reference.txt
