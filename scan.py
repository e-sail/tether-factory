#! /usr/bin/env python
"""\
Scan for serial ports.

"""

import serial
import glob
import os



def windowsScan():
    """scan for available ports. return a list of available portnames"""
    open_ports = []
    for i in range(0,10):
        try:
            s = serial.Serial(i)
            open_ports.append(s.portstr)
            s.close()   # explicit close 'cause of delayed GC in java
        except serial.SerialException:
            pass
    return open_ports

def linuxScan():
    """scan for available ports. return a list of device names."""
    available = []
    available.append(glob.glob('/dev/ttyUSB*'))
    available.append(glob.glob('/dev/ttyACM*'))
    for i in available:
        try:
            s = serial.Serial(i)
            open_ports.append(s.portstr)
            s.close()   # explicit close 'cause of delayed GC in java
        except serial.SerialException:
            pass
    return open_ports


def ports():
    if os.name == 'posix':
        linuxScan()
        
        print "Found ports:"
        for name in linuxScan():
            print name
        return str(linuxScan())
    
    elif os.name == 'nt':
        #windowsScan()
        
        #print "Found ports:"
        #for s in windowsScan():
        #    print "%s" % (s)
        return windowsScan()

if __name__=='__main__':
    openports = ports()
    for x in range(len(openports)):
        print openports.pop()
    
    
    