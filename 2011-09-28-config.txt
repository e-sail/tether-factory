[Parameters]
clamp2open = 110
spoolsidefreearea = 18000
flattenerarmin = 130
stepperrpm = 5
pinon = 115
spoolsiderpm = 40
flatteneroff = 102
flatteneron = 80
tensionon = 29
clamp2close = 180
pinoff = 125
spoolsteps = 60
flattenerarmout = 15
clamp1close = 130
clamp1open = 90
spoolsidesteps = 50
stepdelay = 200
tensionoff = 40

